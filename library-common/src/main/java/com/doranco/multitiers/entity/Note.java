package com.doranco.multitiers.entity;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
//@Table (name="myl_note" , uniqueConstraints={ @UniqueConstraint ( columnNames= { "book_id" } ) , @UniqueConstraint ( columnNames= { "user_id" } ) })
@Table (name="myl_note")
@IdClass (IdNote.class)
public class Note implements Serializable  {

	private static final long serialVersionUID = 8990184735936604309L;

	@Id @ManyToOne @NotNull	private User user;
	@Id @ManyToOne @NotNull private Book book;

	
	//@NotNull 		
	private Date noteDate;
	
	@NotNull 		
	@Min(0) @Max(5)
	private int  noteValue;
	
	@Size(min=0, max=240) 
	private String      noteComment;
	
	// constructeur
	public Note() { super();}

	// Get /Set
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}

	public Date getNoteDate() {
		return noteDate;
	}
	public void setNoteDate(Date noteDate) {
		this.noteDate = noteDate;
	}

	public int getNoteValue() {
		return noteValue;
	}
	public void setNoteValue(int noteValue) {
		this.noteValue = noteValue;
	}


	public String getNoteComment() {
		return noteComment;
	}
	public void setNoteComment(String noteComment) {
		this.noteComment = noteComment;
	}
	
	


	@Override
	public String toString() {
		return "Note [user=" + user + ", book=" + book + ", noteDate=" + noteDate + ", noteValue=" + noteValue
				+ ", noteComment=" + noteComment + "]";
	}




}
