package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;




@Entity
@Table (name="myl_user")
public class User extends Identifier implements Serializable{

	private static final long serialVersionUID = -7377543681120824971L;
	
	@NotNull 
	//@Column(unique=true)// a tester
	private String userName;
	
	@NotNull 
	@Pattern(regexp="^[a-zA-Z0-9_]*$") // Regex : Pour faire un controle de champ uniquement alpha numerique	
	@Size (min=6)
	private String password;
	
	private String firstName;   // possible de definir la taille par 	@Column(length=40)
	
	@NotNull 
	private String lastName;
	
	
	private boolean admin      = false;
	private boolean superAdmin = false;
	
	

	
	// Constructeur
	public User() { super();}

	
	// Get / Set
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isSuperAdmin() {
		return superAdmin;
	}


	@Override
	public String toString() {
		return "User [ id=" + id +" userName=" + userName + ", password=" + password + ", firstName=" + firstName + ", lastName="
				+ lastName + ", admin=" + admin + ", superAdmin=" + superAdmin + "]";
	}

	
}
