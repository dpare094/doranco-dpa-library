package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;


@Entity
@Table (name="myl_viewing")
@IdClass (IdView.class)
public class Viewing  implements Serializable {

	private static final long serialVersionUID = -5300225215567284874L;
	
	@Id @ManyToOne private User user;
	@Id @ManyToOne private Book book;
	
	private Date startDate;
	
	@Min(1)
	@Max(4)
	private long duration; // en heures
		
	
	// Constructeur
	public Viewing() { super();}

	// GET/SET
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}

	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}

	
	@Override
	public String toString() {
		return "Viewing [user=" + user + ", book=" + book + ", startDate=" + startDate + ", duration=" + duration + "]";
	}
	
	
}
