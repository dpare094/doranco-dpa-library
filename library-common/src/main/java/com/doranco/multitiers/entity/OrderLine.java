package com.doranco.multitiers.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name="myl_order_line")
@IdClass (IdOrderLine.class)
public class OrderLine implements Serializable {


	private static final long serialVersionUID = -4674180625983147507L;

	@Id @ManyToOne private Order order;
	@Id @ManyToOne private Book  book;

	
	// constructeur
	public OrderLine() { super();}

	// Get/SET
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}

	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}

	@Override
	public String toString() {
//		return "OrderLine [order=" + order + ", book=" + book + "]";
		return "OrderLine [ book=" + book + "]";
	}
	

	
	
}
