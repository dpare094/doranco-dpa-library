package com.doranco.multitiers.entity;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table (name="myl_book")
public class Book extends Identifier implements Serializable{
	
	private static final long serialVersionUID = 4484249362256718453L;
	
	@NotNull 
	@Pattern(regexp="^\\d{9}[\\d|X]$") // 10 caractère finissant pas X
	//@Column(unique=true)// a tester
	private String isbn; 
	
	@NotNull  private String title;
	
	private String image;
	
	@NotNull @Lob @Column(length=512)
	private String preface; 
	
	@NotNull
	@Lob  // enregistrer en bd sous forme de blob
	private byte[] content; // le contenu du fichier
	
	// Au choix entre les deux possibles
	@Transient	private long averageNote;
	

	
	// Constructeur
	public Book() { super(); }

	//Get / Set

	
	public String getTitle() {
		return title;
	}
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}

	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	

	public String getPreface() {
		return preface;
	}
	public void setPreface(String preface) {
		this.preface = preface;
	}

	public long getAverageNote() {
		return averageNote;
	}

	public void setAverageNote(long averageNote) {
		this.averageNote = averageNote;
	}

	@Override
	public String toString() {
		return "Book [isbn=" + isbn + ", title=" + title + ", image=" + image + ", preface=" + preface + ", content="
				+ Arrays.toString(content) + ", averageNote=" + averageNote + "]";
	}

	

	
//	@Override
//	public String toString() {
//		return "Book [ID=" + id +" ISBN=" + ISBN + ", title=" + title +  ", image=" + image + ", content=" + Arrays.toString(content) + "]";
//	}
//

	
	
}
