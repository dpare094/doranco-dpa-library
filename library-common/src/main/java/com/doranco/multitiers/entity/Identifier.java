package com.doranco.multitiers.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;


@MappedSuperclass
public class Identifier implements Serializable {

	private static final long serialVersionUID = -8880835640947562558L;
	
	
	@Id	
	@GeneratedValue (strategy = GenerationType.AUTO) 
	//@NotNull 
	protected long id;
	
	//constructeur
	public Identifier() {super();}
	
	// Get/set
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Identifier [id=" + id + "]";
	}

	
	
}
