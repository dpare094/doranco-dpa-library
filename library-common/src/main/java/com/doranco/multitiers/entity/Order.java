package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
@Table (name="myl_order")
public class Order extends Identifier implements Serializable{

	private static final long serialVersionUID = -5646343976234349819L;
	
	@ManyToOne	@NotNull private User user;
	
	@Transient	private List<OrderLine> orderLines; 
	
	
	// Constructeur
	public Order() { super();}

	// Get Set
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	public List<OrderLine> getOrderLines() {
		return orderLines;
	}
	public void setOrderLines(List<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	@Override
	public String toString() {
		return "Order [ id=" + id + ",user=" + user + ", orderLines=" + orderLines + "]";
	}

	
	

	
}
