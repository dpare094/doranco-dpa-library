package com.doranco.multitiers.ejb.interfaces;

import javax.ejb.Remote;

import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;

@Remote
public interface IOrder {

	public Order createOrder (Order order) throws LibraryException; 	
		
	public Page<Order> findOrdersByPage (int pageSize, int pageNumber) throws LibraryException;
	public Page<OrderLine> findOrderLinesOrderByPage(long id, int pageSize, int pageNumber) throws LibraryException;
	
}
