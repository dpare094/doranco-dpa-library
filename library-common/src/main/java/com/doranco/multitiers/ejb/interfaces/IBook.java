package com.doranco.multitiers.ejb.interfaces;

import java.util.List;

import javax.ejb.Remote;

import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;


@Remote
public interface IBook {

	public Book createBook(Book book) throws LibraryException; 
	public Book updateBook(Book book) throws LibraryException; 
	public Book deleteBook(long id)   throws LibraryException;
	


	public List<Book>  findAllBooks () throws LibraryException; 	
	public Page<Book>  findBooksByPage (int pageSize, int pageNumber)  throws LibraryException;
	
	public Book        findBookById (long idBook, Long idUser)			throws LibraryException;

	
	public List<Book>  findBooksByTitleOrIsbn(String bookNameIsbn) 	throws LibraryException; 
	public Page<Book>  findBooksByTitleOrIsbn(String bookNameIsbn,  int pageSize, int pageNumber) 	throws LibraryException; 
	
	public Page<Note> findBookNotesByPage(long bookId, int pageSize, int pageNumber) throws Exception;
	public List<Note> findBookNotes(long bookId) throws LibraryException;

	public Page<Viewing> findBookViewingsByPage(long bookId, int pageSize, int pageNumber) throws Exception;

	public long getAverageNoteBook (long bookId)throws LibraryException; 
	public Double getAverageNote(long bookId) throws LibraryException;
}
