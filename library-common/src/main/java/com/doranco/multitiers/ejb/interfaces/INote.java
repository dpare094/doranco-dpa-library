package com.doranco.multitiers.ejb.interfaces;

import java.util.List;

import javax.ejb.Remote;

import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;


@Remote
public interface INote {

	public Note      createNote  (Note note) throws LibraryException;; 
	public Note      updateNote  (Note note) throws LibraryException;; 
	public void 	 deleteNote  (long idUser, long idBook) throws LibraryException;
	
	public Note        findNote (long idUser, long idBook) throws LibraryException; 	
	public Note        findNote (User user, Book book) throws LibraryException; 	
	public List<Note>  findAllNotes    () 	throws LibraryException; 	
	public Page<Note>  findNotesByPage (int pageSize, int pageNumber)  throws LibraryException;
	
	public List<Note> findNotes( long idBook) throws LibraryException;
	
}
