package com.doranco.multitiers.ejb.interfaces;

import java.util.List;
import javax.ejb.Remote;

import com.doranco.multitiers.exceptions.LibraryException;

@Remote
public interface Panier {

	// Gestion de la liste des livres 
	public void addBook(String book) throws LibraryException;	// ajoute un livre à la liste
	public void removeBook(String book) throws LibraryException; // supprime un livre de la liste
	
	public List<String> getBooks() throws LibraryException; // affiche la liste des livres	
	public boolean isBookExist( String book)  throws LibraryException ; // Renvoi vrai si le livre exist 
	
	
}
