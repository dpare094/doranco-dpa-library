package com.doranco.multitiers.ejb.interfaces;


import java.util.List;

import javax.ejb.Remote;

import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.UserNotFoundException;
import com.doranco.multitiers.utils.Page;


@Remote
public interface IUser  {

	public  User       subscribe  (User user) throws LibraryException;
	public  String     connect    (String userName, String password, String uriInfoPath) throws LibraryException; 
	
	public  User       setAsAdmin  (long id) throws LibraryException;

	public List<User>  findAllUser () throws LibraryException; 	
	public Page<User>  findUsersByPage (int pageSize, int pageNumber)  throws LibraryException;
	public User findUser(long id) throws LibraryException;
	
	public Page<Note>    findUserNotesByPage(long userId, int pageSize, int pageNumber) throws LibraryException;
	public Page<Viewing> findUserViewingsByPage(long userId, int pageSize, int pageNumber) throws LibraryException;
	public Page<Order>   findUserOrdersByPage(long userId, int pageSize, int pageNumber) throws LibraryException;

	public User findUserByUserName (String userName) throws LibraryException;
	public User findUserByConnect  (String userName, String password) throws LibraryException; 

}
