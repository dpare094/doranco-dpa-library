package com.doranco.multitiers.ejb.interfaces;

import javax.ejb.Remote;

import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;

@Remote
public interface IViewing {

	public Viewing createViewing(Viewing viewing) 			throws LibraryException;;
	public Viewing updateViewing(Viewing viewing) 			throws LibraryException;;
	public void    deleteViewing(long idUser, long idBook) 	throws LibraryException;

	public Page<Viewing> findViewingsByPage(int pageSize, int pageNumber)	throws LibraryException;
	public Viewing findViewingsByIds(long idUser, long idBook) throws LibraryException ;

}
