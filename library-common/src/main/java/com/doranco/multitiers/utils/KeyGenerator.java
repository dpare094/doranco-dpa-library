package com.doranco.multitiers.utils;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;


public class KeyGenerator {

	private static KeyGenerator INSTANCE; 
	private Key key;
	private KeyStore keyStore;


	private static Logger logger = Logger.getLogger(KeyGenerator.class);

	
	/**
	 * EJB demande la clé en premier 
	 * donc l'exception doit être transferée pour être traitée ailleurs
	 * a destination de celui qui essaie de recupere cette classe -> userBean
	 * 
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 * @throws CertificateException 
	 * @throws KeyStoreException 
	 * @throws UnrecoverableKeyException 
	 * 
	 */
	private KeyGenerator() throws NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException, UnrecoverableKeyException {		
			
		/*
		 * ON A UN PROBLEME AVEC CETTE FACON DE FAIRE:
		 * la clé est générée sur le serveur et sur le client
		 * et ce sont deux instances différentes donc 2 clés différente
		 * alors qu'on a besoi nque ce soit deux clés identiques
		 * 
		 */
	//		key = javax.crypto.KeyGenerator.getInstance("DES").generateKey();
	//		logger.debug("[KeyGenerator] --- GENERATION DE KEY ---  " + key +" ----------");
	
		/*
		 * Avec un fichier , mais on a des pb jwt
		 * 
		 */
//		loadKeyStore();
//		key = keyStore.getKey("keyToken", "doranco".toCharArray());
		
		/*
		 * avec cle statique
		 */
		String keyString = "cleStatique";
		key = new SecretKeySpec(keyString.getBytes(), 0, keyString.getBytes().length, "DES"); 

		if (key != null)
			logger.info("[KeyGenerator]  ----- > " + key.getFormat()+"/" + key.toString());
		else
			logger.info("[KeyGenerator]  ----- > key = NULL");

	}
	
	/**
	 * loadKeyStore : utile pour recup cle dans un fichier
	 * 
	 * @throws KeyStoreException 
	 * @throws IOException 
	 * @throws CertificateException 
	 * @throws NoSuchAlgorithmException 
	 * 
	 */
//	private void loadKeyStore () throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
//	
//		InputStream is =null; 
//		is = this.getClass().getClassLoader().getResourceAsStream("libraryKeyStore");
//		keyStore = KeyStore.getInstance("PKCS12"); 
//		
//		if (is == null) 
//			logger.debug("unable to get file");
//		
//		keyStore.load(is, "doranco".toCharArray());
//
//	}



	/**
	 * on declare le methode static
	 * --> on peut utiliser le nom de la class pour ne pas faire de new
	 * 
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 * @throws CertificateException 
	 * @throws KeyStoreException 
	 * @throws UnrecoverableKeyException 
	 * 
	 */
	public static KeyGenerator getINSTANCE() throws NoSuchAlgorithmException, UnrecoverableKeyException, KeyStoreException, CertificateException, IOException {
		
		// retourne une instance non null  --> comme pour les annotation comme @EJB, ...
		if (INSTANCE == null) 
			INSTANCE = new KeyGenerator(); 
		return INSTANCE;
	}

	/**
	 * 
	 * @return
	 */
	public Key getKey() {
		return key;
	}	
		
}
