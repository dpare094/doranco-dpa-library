package com.doranco.multitiers.utils;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author stagiaire
 *
 * Permet de decouper l'affichage en page d'un certain nombre d'elements
 * Paramêtre typé pour utiliser toutes les classes possibles
 * 
 */
public class Page<T>  implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 3315581456284882353L;
	
	// élements envoyés au metier
	private int pageSize; 
	private int pageNumber;
	// elements renvoyés par le metier
	private long totalCount;
	private List<T> content;
	
	//Constructeurs
	public Page() {super();	}
	
	public Page(int pageSize, int pageNumber, long totalCount, List<T> content) {
		super();
		this.pageSize = pageSize;
		this.pageNumber = pageNumber;
		this.totalCount = totalCount;
		this.content = content;
	}


	// GET/SET
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}


	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getContent() {
		return content;
	}
	public void setContent(List<T> content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Page [pageSize=" + pageSize + ", pageNumber=" + pageNumber + ", totalCount=" + totalCount + ", content="
				+ content + "]";
	}	
	
}
