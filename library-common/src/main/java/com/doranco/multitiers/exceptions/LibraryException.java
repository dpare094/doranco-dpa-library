package com.doranco.multitiers.exceptions;

public class LibraryException extends Exception {

	//private static long serialVersionUID= 1L;
	
	private static final long serialVersionUID = 8492546459142755700L;

	public LibraryException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LibraryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public LibraryException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public LibraryException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public LibraryException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
