

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books on the Library</title>
</head>
<body>
	<h1 align=center>Mes livres</h1>
	<table align="center" style="border: 1px solid;">
		<tr>
			<td width=400px>
				<h2>La liste des livres...</h2>

				<form action="${project.name}" method="get">
					<ul type="none">

						<c:forEach items="${books}" var="book">
							<li ><INPUT type="radio" name="theBookRemove" value="${book}" />${book}
							</li>
						</c:forEach>
						<br />
						<input name="removeBook" type=submit value="Supprimer un Livre"/>
					</ul>
				</form>
			</td>
			<td width=500px>
				<form action="${project.name}" method="get">
					<table style="border: 1px solid;" width=500px>
						<tr>
							<td colspan="2">
								<h2>Ajouter un livre</h2>
							</td>
						</tr>
						<tr>
							<td>livre :</td>
							<td><input type="text" name="theBookAdd" /></td>
							<td><input name="addBook" type=submit
								value="Ajouter un Livre"  /></td>
						</tr>
					</table>
				</form>
				<form action="${project.name}" method="get">
					<table style="border: 1px solid;" width=500px>
						<tr>
						<tr>
							<td colspan="2">
								<h2>Supprimer un livre</h2>
							</td>
						</tr>
						<tr>
							<td>livre :</td>
							<td><input type="text" name="theBookRemove" /></td>
							<td><input name="removeBookNom" type=submit
								value="Supprimer un Livre"  /></td>
						</tr>
						</tr>
					</table>
				</form>

			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="border: 1px solid;">
				<i>${message}</i>
			</td>
		</tr>
	</table>
</body>
</html>