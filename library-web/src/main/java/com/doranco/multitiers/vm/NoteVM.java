package com.doranco.multitiers.vm;

import java.io.Serializable;
import java.util.Date;

public class NoteVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3347128744852757006L;
	
	private long idUser;
	private long idBook;
	
	private Date   noteDate;
	private int    noteValue;
	private String noteComment;
	
	private String firstName; 
	private String lastName; 
	private String userN;
	
	
	// Constructeur
	public NoteVM() {super();}
	
	// GET /SET	
	public Date getNoteDate() {
		return noteDate;
	}
	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public long getIdBook() {
		return idBook;
	}

	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}

	public void setNoteDate(Date noteDate) {
		this.noteDate = noteDate;
	}
	
	public int getNoteValue() {
		return noteValue;
	}
	public void setNoteValue(int noteValue) {
		this.noteValue = noteValue;
	}
	
	public String getNoteComment() {
		return noteComment;
	}
	public void setNoteComment(String noteComment) {
		this.noteComment = noteComment;
	}

	
	public String getUserN() {
		return userN;
	}

	public void setUserN(String userN) {
		this.userN = userN;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "NoteVM [idUser=" + idUser + ", idBook=" + idBook + ", noteDate=" + noteDate + ", noteValue=" + noteValue
				+ ", noteComment=" + noteComment + ", firstName=" + firstName + ", lastName=" + lastName + ", userN="
				+ userN + "]";
	}


	
	


	
	
}
