package com.doranco.multitiers.vm;

import java.io.Serializable;
import java.util.Date;

public class ViewingVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2897328168462454523L;
	
	private long idUser; // on ne mets pas l'id_user car on ne veut pas forcement car on ne veut pas forcement l'avoir depuis un user	
	private long idBook; 
	private Date startDate;
	private long duration;
	
	
	// Constructeur
	public ViewingVM() {super();}

	// GET /SET	
	public Date getStartDate() {
		return startDate;
	}
	public long getIdBook() {
		return idBook;
	}

	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	@Override
	public String toString() {
		return "ViewingVM [idUser=" + idUser + ", idBook=" + idBook + ", startDate=" + startDate + ", duration="
				+ duration + "]";
	} 

	

	
}
