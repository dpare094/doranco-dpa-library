package com.doranco.multitiers.vm;

import java.io.Serializable;
import java.util.Arrays;


public class BookVM implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2021544765565312881L;

	
	private long id;
	
	private String isbn; 
	private String title;
	private String image;
	private String preface;
	private byte[] content;	
	// pour la gestion des moyennes des notes [ 2 façons de faire ]
	private long averageNote; // A moi avec transient dans entity Book
	private Double mean;
	

	// Constructeur
	public BookVM() {super();}
	
	// GET / SET

	
	public String getTitle() {
		return title;
	}
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}	

	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
		

	public String getPreface() {
		return preface;
	}
	public void setPreface(String preface) {
		this.preface = preface;
	}

	public long getAverageNote() {
		return averageNote;
	}

	public void setAverageNote(long averageNote) {
		this.averageNote = averageNote;
	}
	
	public Double getMean() {
		return mean;
	}

	public void setMean(Double mean) {
		this.mean = mean;
	}

	@Override
	public String toString() {
		return "BookVM [id=" + id + ", isbn=" + isbn + ", title=" + title + ", image=" + image + ", preface=" + preface
				+ ", content=" + Arrays.toString(content) + ", averageNote=" + averageNote + ", mean=" + mean + "]";
	}


	
}
