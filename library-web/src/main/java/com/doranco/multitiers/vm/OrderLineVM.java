package com.doranco.multitiers.vm;

import java.io.Serializable;

public class OrderLineVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8100879276206073795L;
	
	private long idBook;

	// Constructeur
	public OrderLineVM() {super();}

	// GET /SET
	public long getIdBook() {
		return idBook;
	}
	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}

	@Override
	public String toString() {
		return "OrderLineVM [idBook=" + idBook + "]";
	}	

	
}
