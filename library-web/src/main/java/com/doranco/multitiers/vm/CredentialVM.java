package com.doranco.multitiers.vm;

import java.io.Serializable;

public class CredentialVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -119251005418402054L;
	
	private String userName;
	private String password;
	
	// Constructeur
	public CredentialVM() {super();}

	// GET /SET 
	public String getPassword() {
		return password;
	}
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
