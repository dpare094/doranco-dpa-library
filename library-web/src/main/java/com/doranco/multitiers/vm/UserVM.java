package com.doranco.multitiers.vm;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Transient;

import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Order;


/**
 * 
 * @author stagiaire Class de viewModele de User
 */
public class UserVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7452664802845205833L;
	
	private long    id;
	private String  userName;
	private String  firstName;
	private String  lastName;
	private boolean admin;
	private boolean superAdmin;
	private String  password; // on ajoute password pour la creation

	// Constructeur
	public UserVM() {super();}

	// GET /SET
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}


	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}


	
	@Override
	public String toString() {
		return "UserVM [id=" + id + ", userName=" + userName + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", admin=" + admin + ", superAdmin=" + superAdmin + ", password=" + password + "]";
	}
}