package com.doranco.multitiers.vm;

import java.io.Serializable;
import java.util.List;


public class OrderVM implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7875975841451996608L;
	
	private long id;
	private long idUser;
	
	private List<OrderLineVM> orderLines;
	
	// Constructeur
	public OrderVM() {super();}

	// GET / SET
	
	public long getIdOrder() {
		return id;
	}
	public void setIdOrder(long idOrder) {
		this.id = idOrder;
	}
	
	public long getIdUser() {
		return idUser;
	}
	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public List<OrderLineVM> getOrderLines() {
		return orderLines;
	}
	public void setOrderLines(List<OrderLineVM> orderLines) {
		this.orderLines = orderLines;
	}

	@Override
	public String toString() {
		return "OrderVM [id=" + id + ", idUser=" + idUser + ", orderLines=" + orderLines + "]";
	}


	
}
