package com.doranco.multitiers.resource.exception.mappers;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;

public class NotAuthorizedMapper implements ExceptionMapper<NotAuthorizedException> {
	
	private static Logger logger = Logger.getLogger(NotAuthorizedMapper.class);

	@Override
	public Response toResponse(NotAuthorizedException exception) {
		String MESSAGE = "Vous n'avez pas les autorisations necessaires pour atteindre cette ressource";
		
		logger.error(MESSAGE, exception);	
		ResponseBuilder reponse = Response.status(Status.UNAUTHORIZED).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, MESSAGE); 

		return reponse.build(); 
	}

}
