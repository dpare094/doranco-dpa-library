package com.doranco.multitiers.resource;

import java.net.URI;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IViewing;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.mappers.ViewingMapper;
import com.doranco.multitiers.resource.security.AuthenticationChecked;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.ViewingVM;

@Path("view")
@Produces(MediaType.APPLICATION_JSON)
public class ViewingResource {
	
	/**
	 * 
	 */
	private IViewing iViewing;
	private ViewingMapper viewingMapper = ViewingMapper.INSTANCE;
	private static Logger logger = Logger.getLogger(ViewingResource.class);

	@Context
	UriInfo uriInfo; // pour les info de l'uri

	/**
	 * 
	 * @throws LibraryException
	 */
	public ViewingResource() throws LibraryException {
		super();
		try {
			
			iViewing = (IViewing) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/ViewingBean");

		} catch (NamingException e) {
			logger.error("[ViewingResource/constructeur]Impossible de démarrer la ressource Viewing", e);
			throw new LibraryException(e);
		}
	}

	
	/**
	 * 
	 * @param viewingVm
	 * @return
	 * @throws LibraryException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationChecked 
	public Response createViewing(ViewingVM viewingVm) throws LibraryException {

		Viewing viewing = null;
		
		viewing = viewingMapper.vmToEntity(viewingVm);
		viewing = this.iViewing.createViewing(viewing);

		viewingVm = viewingMapper.entityToVm(viewing);
		URI uri = UriBuilder.fromResource(ViewingResource.class).build();
		return Response.created(uri).entity(viewingVm).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Demande consultation enregistréée")
				.build();

	}

	/**
	 * 
	 * @param viewingVm
	 * @return
	 * @throws LibraryException
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationChecked 
	public Response updateViewing(ViewingVM viewingVm) throws LibraryException {

		Viewing viewing = null;

		viewing = viewingMapper.vmToEntity(viewingVm);
		viewing = this.iViewing.updateViewing(viewing);

		viewingVm = viewingMapper.entityToVm(viewing);

		return Response.ok().entity(viewingVm).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Demande consultation modifiée").build();

	}

	/**
	 * 
	 * @param idOrder
	 * @param idBook
	 * @return
	 * @throws LibraryException
	 */
	@DELETE
	@AuthenticationChecked 
	public Response deleteViewing(@QueryParam("idUser") long idUser, @QueryParam("idBook") long idBook) 
	throws LibraryException {
			
		this.iViewing.deleteViewing(idUser, idBook);

		return Response.ok().header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Demande consultation supprimée").build();

	}

	/**
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws LibraryException
	 */
	@GET
	@Path("allviews")
	public Response getViewingsByPage(@QueryParam("pageSize") int pageSize, @QueryParam("pageNumber") int pageNumber)
	throws LibraryException {

		Page<Viewing>   pages = null;
		Page<ViewingVM> thePagesVM = null;

		pages = this.iViewing.findViewingsByPage(pageSize, pageNumber);
		thePagesVM = viewingMapper.entitiesPageToVMPage(pages);

		return Response.ok().entity(thePagesVM).build();
	}
	
	@GET
	public Response findViewingsByIds(@QueryParam("idUser") int idUser, @QueryParam("idBook") int idBook)
	throws LibraryException {
		
		Viewing viewing = null; 
		viewing = this.iViewing.findViewingsByIds (idUser, idBook); 
		ViewingVM viewingVm = viewingMapper.entityToVm(viewing);
		
		return Response.ok().entity(viewingVm).build();

	}

}
