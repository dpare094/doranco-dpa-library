package com.doranco.multitiers.resource;

import java.net.URI;
import java.util.List;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.INote;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.mappers.NoteMapper;
import com.doranco.multitiers.resource.security.AuthenticationChecked;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.NoteVM;

@Path("notes")
@Produces(MediaType.APPLICATION_JSON)
public class NoteResource {

	/**
	 * 
	 */
	private INote iNote;
	private NoteMapper noteMapper = NoteMapper.INSTANCE;
	private static Logger logger = Logger.getLogger(NoteResource.class);

	@Context
	UriInfo uriInfo; // pour les info de l'uri

	/**
	 * Constructeur Recupere le iUser par un bean a travers JNDI
	 * 
	 * @throws LibraryException
	 */
	public NoteResource() throws LibraryException {
		super();
		try {
			iNote = (INote) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/NoteBean");

		} catch (NamingException e) {
			logger.error("[NoteResource/constructeur]Impossible de démarrer la ressource NOTE", e);
			throw new LibraryException(e);
		}
	}

	/**
	 * 
	 * @param noteVm
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationChecked 
	public Response createNote(NoteVM noteVm) throws LibraryException {

		Note note = null;

		note = noteMapper.vmToEntity(noteVm);
		note = this.iNote.createNote(note);

		noteVm = noteMapper.entityToVm(note);
		URI uri = UriBuilder.fromResource(NoteResource.class).build();
		return Response.created(uri).entity(noteVm).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Note creee")
				.build();

	}

	/**
	 * 
	 * @param noteVm
	 * @return
	 * @throws LibraryException
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationChecked 
	public Response updateNote(NoteVM noteVm) throws LibraryException {

		Note note = null;

		note = noteMapper.vmToEntity(noteVm);
		note = this.iNote.updateNote(note);

		noteVm = noteMapper.entityToVm(note);

		return Response.ok().entity(noteVm).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Note modifie").build();

	}

	@DELETE
	@AuthenticationChecked 
	public Response deleteNote(@QueryParam("idUser") long idUser, @QueryParam("idBook") long idBook) 
	throws LibraryException {
			
		this.iNote.deleteNote(idUser, idBook);

		return Response.ok().header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Note supprimee").build();

	}

	/**
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	@GET
	public Response getNotesByPage(@QueryParam("pageSize") int pageSize, @QueryParam("pageNumber") int pageNumber)
	throws LibraryException {

		Page<Note> pages = null;
		Page<NoteVM> thePagesVM = null;

		pages = this.iNote.findNotesByPage(pageSize, pageNumber);
		thePagesVM = noteMapper.entitiesPageToVMPage(pages);

		return Response.ok().entity(thePagesVM).build();
	}

	/**
	 * 
	 * @return
	 * @throws LibraryException
	 */
	@GET
	@Path("allNotes")
	public Response getAllNote() throws LibraryException {
		List<Note> notes = null;
		List<NoteVM> theNotesVM = null;

		notes = this.iNote.findAllNotes();
		theNotesVM = noteMapper.entitiesToVm(notes); // userMapper definie dans les variables

		GenericEntity<List<NoteVM>> entity = new GenericEntity<List<NoteVM>>(theNotesVM) {
		};
		return Response.ok().entity(entity).build();

	}

	@GET
	@Path("oneNote")
	public Response getNote(@QueryParam("idUser") int idUser, @QueryParam("idBook") int idBook)
	throws LibraryException {

		NoteVM noteVm = null;
		Note note = null;

//		noteVm.setIdUser(idUser);
//		noteVm.setIdBook(idBook);

		note = noteMapper.vmToEntity(noteVm);
//		note = this.iNote.findNote(note.getUser(), note.getBook());
		note = this.iNote.findNote(idUser, idBook);

		noteVm = noteMapper.entityToVm(note);

		return Response.ok().entity(noteVm).header("Server message", "Note modifie").build();

	}

	@GET
	@Path("notesByBook/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNotesByBook(@PathParam("id") long idBook)
	throws LibraryException {

		List<NoteVM> noteVm ;		
		List<Note> notes ;

		notes = this.iNote.findNotes(idBook);
		noteVm = noteMapper.entitiesToVm(notes);

//		for (int i = 0; i< notes.size(); i++) {
//			User user = notes.get(i).getUser();
//			noteVm.get(i).setFirstLastName(user.getFirstName()+" "+user.getLastName());	
//		}

		GenericEntity<List<NoteVM>> entity = new GenericEntity<List<NoteVM>>(noteVm) {};

		return Response.ok().entity(entity).header("Server message", "Notes trouvee pour un livre").build();

	}

	
}
