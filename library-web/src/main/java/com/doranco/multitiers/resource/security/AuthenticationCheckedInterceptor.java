package com.doranco.multitiers.resource.security;

import java.io.IOException;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;

import org.apache.log4j.Logger;

import com.doranco.multitiers.utils.KeyGenerator;

import io.jsonwebtoken.Jwts;

public class AuthenticationCheckedInterceptor implements ContainerRequestFilter {

	private static Logger logger = Logger.getLogger(AuthenticationCheckedInterceptor.class);

	/**
	 * 
	 * throws IOException est definie dans la methode de base, meme si aucune
	 * exception retournées
	 */
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		// recuperer la requete envoyée, recup du header
		String autorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

		// Verifier que la header contient bien la base d'identification
		if ((autorizationHeader == null) || (!autorizationHeader.startsWith("Bearer "))) {
			logger.error("[filter]Invalid autorizationHeader");
			throw new NotAuthorizedException("L'entete authorisation devrait être fournie");

		}

		// recuperer le token qui s'y trouve, en supprimant le 'Bearer '
		String token = autorizationHeader.substring("Bearer".length()).trim();

		Key key;
		try {
			
			key = KeyGenerator.getINSTANCE().getKey();
	//		System.out.println("=====> la cle que l'on a pour controle : " + key.getFormat());
	//		logger.info("[filter]  ----- > " + key.getFormat() + "/" + key.toString());

			Jwts.parser().setSigningKey(key).parseClaimsJws(token);

			
		} catch (UnrecoverableKeyException e) {
			logger.error("[filter]/ UnrecoverableKeyException /erreur lors de la validation de la key(token)", e);
	
		} catch (NoSuchAlgorithmException e) {
			logger.error("[filter]/ NoSuchAlgorithmException /erreur lors de la validation de la key(token)", e);
		
		} catch (KeyStoreException e) {
			logger.error("[filter]/ KeyStoreException /erreur lors de la validation de la key(token)", e);
		
		} catch (CertificateException e) {
			logger.error("[filter]/ CertificateException /erreur lors de la validation de la key(token)", e);
		
		}

		// logger.error("[filter]erreur lors de la validation de la key(token)", e);
		// ne doit jamais arriver, car on ne passera pas si le token n'a pas été generé

	//	logger.debug("[filter]valid token++++---> " + token);
	}

}
