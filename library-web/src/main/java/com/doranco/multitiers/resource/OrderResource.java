package com.doranco.multitiers.resource;

import java.net.URI;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IOrder;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.mappers.OrderLineMapper;
import com.doranco.multitiers.mappers.OrderMapper;
import com.doranco.multitiers.resource.security.AuthenticationChecked;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.OrderLineVM;
import com.doranco.multitiers.vm.OrderVM;

@Path("order")
@Produces(MediaType.APPLICATION_JSON)
public class OrderResource {

	/**
	 * 
	 */
	private IOrder iOrder;
	private OrderMapper orderMapper = OrderMapper.INSTANCE;
	private OrderLineMapper orderLineMapper = OrderLineMapper.INSTANCE;

	private static Logger logger = Logger.getLogger(OrderResource.class);

	@Context
	UriInfo uriInfo; // pour les info de l'uri

	/**
	 * 
	 * @throws LibraryException
	 */
	public OrderResource() throws LibraryException {
		super();
		try {
			iOrder = (IOrder) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/OrderBean");

		} catch (NamingException e) {
			logger.error("Impossible de démarrer la ressource Order", e);
			throw new LibraryException(e);
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationChecked
	public Response createOrder(OrderVM orderVm) throws LibraryException {

		Order order = null;

		order = orderMapper.vmToEntity(orderVm);
		order = this.iOrder.createOrder(order);

		orderVm = orderMapper.entityToVm(order);

		URI uri = UriBuilder.fromResource(OrderResource.class).build();
		return Response.created(uri).entity(orderVm).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Commande créée")
				.build();

	}

	/**
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws LibraryException
	 */
	@GET
	public Response getAllUsersByPage(@QueryParam("pageSize") int pageSize, @QueryParam("pageNumber") int pageNumber)
			throws LibraryException {

		Page<Order> pages = null;
		Page<OrderVM> thePagesVM = null;

		pages = this.iOrder.findOrdersByPage(pageSize, pageNumber);
		thePagesVM = orderMapper.entitiesPageToVMPage(pages);

		return Response.ok().entity(thePagesVM).build();

	}

	/**
	 * 
	 * @param id
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws LibraryException
	 */
	@GET
	@Path("{id}")
	public Response getOrderLinesOrderByPage(
			@PathParam("id") long id, 
			@QueryParam("pageSize") int pageSize, 
			@QueryParam("pageNumber") int pageNumber)
		throws LibraryException {
		
		Page<OrderLine> pages = null; 
		Page<OrderLineVM> thePagesVM = null; 
		
		pages = this.iOrder.findOrderLinesOrderByPage(id,pageSize, pageNumber);
		thePagesVM = orderLineMapper.entitiesPageToVMPage(pages);
	
		return Response.ok().entity(thePagesVM).build();
		
	}
	
	
}
