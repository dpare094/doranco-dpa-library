package com.doranco.multitiers.resource;

import java.io.InputStream;
import java.net.URI;
import java.util.List;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IBook;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.mappers.BookMapper;
import com.doranco.multitiers.mappers.NoteMapper;
import com.doranco.multitiers.resource.security.AdminAuthenticationChecked;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.BookVM;
import com.doranco.multitiers.vm.NoteVM;

@Path("books")
@Produces(MediaType.APPLICATION_JSON) // permet a Jax-rx va comprendre que c'est ce qu'on veut retourner
										// va chercher un message bodywrite qu'il va convertir en Json
public class BookResource {

	/**
	 * 
	 */
	private IBook iBook;
	private BookMapper bookMapper = BookMapper.INSTANCE;
	private static Logger logger = Logger.getLogger(BookResource.class);

	@Context
	UriInfo uriInfo; // pour les info de l'uri

	/**
	 * Constructeur Recupere le iUser par un bean a travers JNDI
	 * 
	 * @throws LibraryException
	 */
	public BookResource() throws LibraryException {
		super();
		try {
			iBook = (IBook) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/BookBean");

		} catch (NamingException e) {
			logger.error("Impossible de démarrer la ressource BOOK", e);
			throw new LibraryException(e);
		}
	}

	/**
	 * 
	 * @param bookVm
	 * @return
	 */
	/*
	 * @POST
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON) //@AdminAuthenticationChecked public
	 * Response createBook(BookVM bookVm) throws LibraryException {
	 * 
	 * Book book = null;
	 * 
	 * book = bookMapper.vmToEntity(bookVm); book = this.iBook.createBook(book);
	 * 
	 * bookVm = bookMapper.entityToVm(book);
	 * 
	 * URI uri = UriBuilder.fromResource(BookResource.class).build(); return
	 * Response.created(uri).entity(bookVm).header(LibraryConstants.
	 * RESPONSE_HEADER_MESSAGE, "Livre créé").build();
	 * 
	 * //return Response.ok().entity(bookVm).header("Server message",
	 * "Livre cree").build(); }
	 */
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@AdminAuthenticationChecked
	public Response createBook(@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetails,
			@FormDataParam("bookMetaData") FormDataBodyPart jsonPart) throws LibraryException, Exception {

		jsonPart.setMediaType(MediaType.APPLICATION_JSON_TYPE);
		BookVM bookVm = jsonPart.getValueAs(BookVM.class);

		byte[] fileContent = IOUtils.toByteArray(fileInputStream);
		bookVm.setContent(fileContent);

		bookVm = bookMapper.entityToVm(this.iBook.createBook(bookMapper.vmToEntity(bookVm)));

		URI uri = UriBuilder.fromResource(BookResource.class).build();
		return Response.created(uri).entity(bookVm).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Livre créé")
				.build();

	}

	/**
	 * 
	 * @param bookVm
	 * @return
	 * @throws LibraryException
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@AdminAuthenticationChecked // test asAdmin
	public Response updateBook(BookVM bookVm) throws LibraryException {

		Book book = null;

		book = bookMapper.vmToEntity(bookVm);
		book = this.iBook.updateBook(book);

		bookVm = bookMapper.entityToVm(book);
		return Response.ok().entity(bookVm).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Livre mis a jour")
				.build();

	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws LibraryException
	 */
	@DELETE
	@Path("{id}")
	@AdminAuthenticationChecked // test asAdmin
	public Response deleteBook(@PathParam("id") long id) throws LibraryException {

		this.iBook.deleteBook(id);
		return Response.ok().header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Livre supprime").build();

	}

	/**
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	@GET
	public Response getBooksByPage(@QueryParam("pageSize") int pageSize, @QueryParam("pageNumber") int pageNumber)
			throws LibraryException {

		Page<Book> pages = null;
		Page<BookVM> thePagesVM = null;

		pages = this.iBook.findBooksByPage(pageSize, pageNumber);
		thePagesVM = bookMapper.entitiesPageToVMPage(pages); // userMapper definie dans les variables

		return Response.ok().entity(thePagesVM).build();
	}

	/**
	 * 
	 * @return
	 * @throws LibraryException
	 */
	@GET
	@Path("allBooks")
	public Response getAllBooksVM() throws LibraryException {

		List<Book> theBooks = null;
		List<BookVM> theBooksVM = null;

		theBooks = this.iBook.findAllBooks();
		theBooksVM = bookMapper.entitiesToVm(theBooks); // userMapper definie dans les variables
		for (BookVM bookVM : theBooksVM) {
			bookVM.setMean(this.iBook.getAverageNote(bookVM.getId()));
			
		}

		GenericEntity<List<BookVM>> entity = new GenericEntity<List<BookVM>>(theBooksVM) {
		};

		return Response.ok().entity(entity).build();

	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws LibraryException
	 */
	@GET
	@Path("{id}")
	public Response getBook(@PathParam("id") long id) throws LibraryException {

		BookVM bookVM = null;
		Book book = null;

		book = iBook.findBookById(id, null);
		bookVM = bookMapper.entityToVm(book);

		return Response.ok().entity(bookVM).build();

	}

	/**
	 * 
	 * @param idBook
	 * @param idUser
	 * @return
	 * @throws LibraryException
	 */
	@GET
	@Path("viewing")
	public Response getViewingBook(@QueryParam("idBook") long idBook, @QueryParam("idUser") long idUser)
			throws LibraryException {

		BookVM bookVM = null;
		Book book = null;

		book = iBook.findBookById(idBook, idUser);
		bookVM = bookMapper.entityToVm(book);

		return Response.ok().entity(bookVM).build();

	}

	/**
	 * 
	 * @param bookTitleIsbn
	 * @return
	 * @throws LibraryException
	 */
	@GET
	@Path("findList")
	public Response getBooksByNameOrIsbn(@QueryParam("titleIsbn") String bookTitleIsbn) throws LibraryException {

		List<Book> theBooks = null;
		List<BookVM> theBooksVM = null;

		theBooks = this.iBook.findBooksByTitleOrIsbn(bookTitleIsbn);
		theBooksVM = bookMapper.entitiesToVm(theBooks); // userMapper definie dans les variables

		GenericEntity<List<BookVM>> entity = new GenericEntity<List<BookVM>>(theBooksVM) {
		};

		return Response.ok().entity(entity).build();

	}

	/**
	 * 
	 * @param bookTitleIsbn
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws LibraryException
	 */
	@GET
	@Path("find")
	public Response getBooksByNameOrIsbn(@QueryParam("titleIsbn") String bookTitleIsbn,
			@QueryParam("pageSize") int pageSize, @QueryParam("pageNumber") int pageNumber) throws LibraryException {

		Page<Book> pages = null;
		Page<BookVM> thePagesVM = null;

		pages = this.iBook.findBooksByTitleOrIsbn(bookTitleIsbn, pageSize, pageNumber);
		thePagesVM = bookMapper.entitiesPageToVMPage(pages); // userMapper definie dans les variables

		return Response.ok().entity(thePagesVM).build();
	}

	
	@GET
	@Path("notes/{id}")
	public Response getBooksNotes(@PathParam("id") long id) throws Exception {

		List<Note> pages = null;
		List<NoteVM> thePagesVM = null;

		NoteMapper noteMapper = NoteMapper.INSTANCE;
		pages = this.iBook.findBookNotes(id);
		
		thePagesVM = noteMapper.entitiesToVm(pages); // userMapper definie dans les variables
		GenericEntity<List<NoteVM>> entity = new GenericEntity<List<NoteVM>>(thePagesVM) {		};

		return Response.ok().entity(entity).build();
	}

}
