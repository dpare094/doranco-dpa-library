package com.doranco.multitiers.resource;

import java.net.URI;
import java.util.List;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IUser;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.UserNotFoundException;
import com.doranco.multitiers.mappers.NoteMapper;
import com.doranco.multitiers.mappers.UserMapper;
import com.doranco.multitiers.resource.security.AuthenticationChecked;
import com.doranco.multitiers.resource.security.SuperAdminAuthenticationChecked;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.CredentialVM;
import com.doranco.multitiers.vm.NoteVM;
import com.doranco.multitiers.vm.UserVM;

@Path("users")
@Produces(MediaType.APPLICATION_JSON) // permet a Jax-rx va comprendre que c'est ce qu'on veut retourner
										// va chercher un message bodywrite qu'il va convertir en Json
public class UserResource {

	/**
	 * //Correspond a @EJB (lookup("java:global/library-ear/library-ejb/UserBean")
	 * //mais RemoteContext fonctionne mieux qd on deploie dans diffèrent serveur
	 */
	private IUser iUser;
	private UserMapper userMapper = UserMapper.INSTANCE;

	private static Logger logger = Logger.getLogger(UserResource.class);

	@Context
	UriInfo uriInfo; // pour les info de l'uri

	/**
	 * Constructeur Recupere le iUser par un bean a travers JNDI
	 * 
	 * @throws LibraryException
	 */
	public UserResource() throws LibraryException {
		super();
		try {
			// nom JNDI ,permet de reperer une ressource java dans un contexte
			// les resources sont demarrés et on un nom jndi.
			// iUser contient tous ce qu'il y a dans UserBean
			iUser = (IUser) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/UserBean");

		} catch (NamingException e) {
			logger.error("[UserResource/constructeur]  Impossible de démarrer la ressource USER", e);
			throw new LibraryException(e);
		}
	}
	
	
	/**
	 * 
	 * @param userVm
	 * @return
	 * @throws LibraryException 
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response newUser(UserVM userVm) throws LibraryException {

		User user = null;

//		try {
			user = userMapper.vmToEntity(userVm);		
			user = this.iUser.subscribe(user);

			userVm = userMapper.entityToVm(user);

//		} catch (LibraryException e) {
//			logger.error("Impossible de creer l'utilisateur", e);
//			return Response.status(Response.Status.NOT_IMPLEMENTED).header("Server message", "Impossible de creer l'utilisateur").build();
//			
//		}
		
		URI uri = UriBuilder.fromResource(UserResource.class).build();
		return Response.created(uri).entity(userVm).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Utilisateur créé").build();
		//return Response.ok().entity(userVm).build();
		//return userVm;
	}

	/**
	 * 
	 * @param connect
	 * @return
	 * @throws UserNotFoundException 
	 * @throws LibraryException 
	 */
	@POST 
	@Path("login") // on donne un path a l'uri pour différentier d'un autre POST. Il faut ajouter le nom à l'uri pour atteindre la page
	public Response userAuthentification(CredentialVM connect) throws LibraryException  {

//		try {

			String token = iUser.connect(connect.getUserName(), connect.getPassword(), uriInfo.getAbsolutePath().toString());

			User user = iUser.findUserByConnect(connect.getUserName(), connect.getPassword());
			UserVM userVm = userMapper.entityToVm(user);
			ResponseBuilder builder = Response.ok();
			
			builder.header(HttpHeaders.AUTHORIZATION, "Bearer " + token).entity(userVm);
		
			return builder.build();
			
			//return Response.ok().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).build(); // la reponse interpretée par le navigateur
		
//		} catch (UserNotFoundException e) {
//			logger.error("[userAuthentification]erreur lors de la connection utilisateur ", e);
//			// mettre un mapper
//			return Response.status(Status.UNAUTHORIZED).header(	public static final String RESPONSE_HEADER_MESSAGE = "Server message";, "Utilisateur/mot de passe incorrect").build();
//
//		}
	}
	
	/**
	 * 
	 * @return
	 * @throws LibraryException
	 */
	@GET
	@Path("setAdmin/{id}")
	@SuperAdminAuthenticationChecked 
	public Response setUserAsAdmin (@PathParam("id") long id) throws LibraryException {

		UserVM userVm; 
		User user = null;
		
		user = this.iUser.setAsAdmin(id);
		userVm = userMapper.entityToVm(user);

		return Response.ok().entity(userVm).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Utilisateur mis à jour en tant qu'admin").build();	

	}
	
	
	/**
	 * @PathParam : paramètre dans uri , avant .../users/{id}
	 * 
	 * @param id
	 * @return
	 * @throws LibraryException 
	 */
	@GET
	@Path("{id}")
	@AuthenticationChecked // ne peut être appeler que si l'user est enregistré
	public Response getUser(@PathParam("id") long id) throws LibraryException {

		UserVM userVM = null;
		User user = null;

//		try {
		user = iUser.findUser( id );
		userVM = userMapper.entityToVm(user);			

		return Response.ok().entity(userVM).build();	

//
// on enleve les try/catch car on va faire un mapper pour libraryException
//									
//		} catch (LibraryException e) {
//			logger.error("[getUser]Erreur recherche d'un utilisateur ", e);
//			return Response.status(Response.Status.REQUESTED_RANGE_NOT_SATISFIABLE).header("Server message", "Erreur recherche d'un utilisateur").build();		
//			
//		}
		
	}


	/**
	 * 
	 * @return
	 * @throws LibraryException 
	 */
	@GET
	@Path("allUsers")
	public Response getAllUsersVM() throws LibraryException {
		
		List<User> theUsers = null;
		List<UserVM> theUsersVM = null;

		theUsers = this.iUser.findAllUser();
		theUsersVM = userMapper.entitiesToVm(theUsers); // userMapper definie dans les variables
		
		GenericEntity<List<UserVM>> entity = new GenericEntity<List<UserVM>>(theUsersVM) {}; 
	
		return Response.ok().entity(entity).build();
	}

	/**
	 * @QueryParam : paramètre après ?
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws LibraryException 
	 */
	@GET
	public Response getAllUsersByPage(
			@QueryParam("pageSize") int pageSize,
			@QueryParam("pageNumber") int pageNumber) 
		throws LibraryException {

		Page<User> pageUsers    = null;
		Page<UserVM> thePagesVM = null;

//		try {
			pageUsers = this.iUser.findUsersByPage(pageSize, pageNumber);
			logger.debug("------pageUsers----->" + pageUsers);
			
			thePagesVM = userMapper.entitiesPageToVMPage(pageUsers); // userMapper definie dans les variables
			logger.debug("------thePagesVM----->" + thePagesVM);
			
			// on  met le resultat dans un objet generic pour ne pas avoir de pb au retour par jax-rs
			//GenericEntity<Page<UserVM>> entity = new GenericEntity<Page<UserVM>>(thePagesVM) {}; 
			//logger.debug("------entity----->" + entity);
			
			//return Response.ok().entity(entity).build();
			return Response.ok().entity(thePagesVM).build();

//		} catch (LibraryException e) {
//			logger.error("[getUsersByPage]Erreur recherche des utilisateurs ", e);
//			return Response.status(Status.NOT_FOUND).build();
//		}
		//return thePagesVM;
	}
	
	
	@GET
	@Path("notes:{id}")
	@AuthenticationChecked // ne peut être appeler que si l'user est enregistré
	public Response getUserNotes(
			@PathParam("id") long id, 
			@QueryParam("pageSize") int pageSize,
			@QueryParam("pageNumber") int pageNumber)
	throws LibraryException {

		NoteMapper noteMapper = NoteMapper.INSTANCE;

		Page<Note> pageUsers    = null;
		Page<NoteVM> pagesVM = null;

		pageUsers = iUser.findUserNotesByPage( id, pageSize, pageNumber );
		pagesVM = noteMapper.entitiesPageToVMPage(pageUsers);			

		return Response.ok().entity(pagesVM).build();	
		
	}
	
	@GET
	@Path("username/{username}")
	public Response getUserByUserName(@PathParam(value = "username") String userName)	throws LibraryException {

		UserVM userVm; 
		User user = null;
		
		user = this.iUser.findUserByUserName(userName);
		userVm = userMapper.entityToVm(user);

		return Response.ok().entity(userVm).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, "Utilisateur trouvé par userName").build();	

		
	}
	
	
	
	
	
}
