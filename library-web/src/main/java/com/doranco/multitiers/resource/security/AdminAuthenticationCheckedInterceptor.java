package com.doranco.multitiers.resource.security;

import java.io.IOException;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.HttpHeaders;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.utils.KeyGenerator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class AdminAuthenticationCheckedInterceptor extends AuthenticationCheckedInterceptor {

	private static Logger logger = Logger.getLogger(AdminAuthenticationCheckedInterceptor.class);

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		super.filter(requestContext);

		String autorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		String token = autorizationHeader.substring("Bearer".length()).trim();

		Key key;
		try {
			key = KeyGenerator.getINSTANCE().getKey();
			
			Claims jwtBody = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody();
			
			List<String> roles = (List<String>) jwtBody.get(LibraryConstants.JWT_ROLE_KEY);	
			if (!roles.contains(LibraryConstants.ADMIN_ROLE) ){
			
				logger.error("[filter]Invalid Role non Admin");
				throw new NotAuthorizedException("La clé role n'est pas admnistrateur");

			}
					
		} catch (UnrecoverableKeyException e) {
			logger.error("[filter]/ UnrecoverableKeyException /erreur lors de la validation de la key(token)", e);
	
		} catch (NoSuchAlgorithmException e) {
			logger.error("[filter]/ NoSuchAlgorithmException /erreur lors de la validation de la key(token)", e);
		
		} catch (KeyStoreException e) {
			logger.error("[filter]/ KeyStoreException /erreur lors de la validation de la key(token)", e);
		
		} catch (CertificateException e) {
			logger.error("[filter]/ CertificateException /erreur lors de la validation de la key(token)", e);
		
		}

	}

}
