package com.doranco.multitiers.resource.exception.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;


public class GeneralExceptionMapper implements ExceptionMapper<Exception>{
	
	
	private static Logger logger = Logger.getLogger(GeneralExceptionMapper.class);

	
	@Override
	public Response toResponse(Exception exception) {
		String MESSAGE = "Probleme General sur l application";
		
		logger.error(MESSAGE, exception);	
		ResponseBuilder reponse = Response.status(Status.INTERNAL_SERVER_ERROR).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, MESSAGE); 

		return reponse.build(); 
	}

}
