package com.doranco.multitiers.resource.security;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

@Provider // c’est cette classe qui enregistre le filtre
public class SecurityFeature implements DynamicFeature {

	/**
	 * DynamicFeature donne des elements qui permet d'ecouter les requetes qui
	 * entrent
	 * 
	 * 
	 */
	@Override
	public void configure(ResourceInfo resourceInfo, FeatureContext context) {

		// si l'annotation est présente on enregistre dans le contexte le filtre
		if (resourceInfo.getResourceMethod().isAnnotationPresent(AuthenticationChecked.class))
			context.register(AuthenticationCheckedInterceptor.class); // enregistrement du filtre réél
		
		if (resourceInfo.getResourceMethod().isAnnotationPresent(AdminAuthenticationChecked.class))
			context.register(AdminAuthenticationCheckedInterceptor.class);
		
		if (resourceInfo.getResourceMethod().isAnnotationPresent(SuperAdminAuthenticationChecked.class))	
			context.register(SuperAdminAuthenticationCheckedInterceptor.class);

	}

}
