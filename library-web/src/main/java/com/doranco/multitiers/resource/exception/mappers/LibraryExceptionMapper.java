package com.doranco.multitiers.resource.exception.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.exceptions.LibraryException;

@Provider
public class LibraryExceptionMapper implements ExceptionMapper<LibraryException> {

	private static Logger logger = Logger.getLogger(LibraryExceptionMapper.class);

	@Override
	public Response toResponse(LibraryException exception) {
		
		String MESSAGE = "Oups ... Grave probleme";
		
		logger.error(MESSAGE, exception);	
		ResponseBuilder reponse = Response.status(Status.NOT_FOUND).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, MESSAGE); 

		return reponse.build(); 
	}

	
	
}
