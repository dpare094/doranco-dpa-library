package com.doranco.multitiers.resource.exception.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.exceptions.UserNotFoundException;


@Provider // pour indiquer a jax-rs que c'est une ressource
public class UserNotFoundMapper implements ExceptionMapper<UserNotFoundException> {

	
	private static Logger logger = Logger.getLogger(UserNotFoundMapper.class);
	
	/**
	 * 
	 */
	@Override
	public Response toResponse(UserNotFoundException exception) {

		String MESSAGE = "Nom d'utilisateur ou mot de passe incorrect";
		
		logger.error(MESSAGE, exception);	
		ResponseBuilder reponse = Response.status(Status.UNAUTHORIZED).header(LibraryConstants.RESPONSE_HEADER_MESSAGE, MESSAGE); 
	
		return reponse.build(); 
	}

}
