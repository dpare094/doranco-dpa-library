package com.doranco.multitiers.context;

import javax.naming.InitialContext;
import javax.naming.NamingException;

 
/**
 *  
 * @author stagiaire
 * 
 * Fournit une et une seule instance quelquesoit le nombre d'utulisateur
 *
 */
  public class RemoteContext {


	private InitialContext initialContext;
	private static RemoteContext instance;
	
	/**
	 * 
	 */
	private RemoteContext() {
		
		try {
			
			// lecture du fichier jndi.properties du contexte
			initialContext =  new InitialContext();
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public InitialContext getInitialContext() {
		return initialContext;
	}
	
	/**
	 *  synchronized gère les accès concurrent
	 * 
	 * @return
	 */
	synchronized public static RemoteContext getInstance() {
		
		if (instance == null) 
			return new RemoteContext();			
		return instance;
	}
	
	

}
