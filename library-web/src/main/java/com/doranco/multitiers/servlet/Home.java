package com.doranco.multitiers.servlet;

import java.io.IOException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.Panier;
import com.doranco.multitiers.exceptions.LibraryException;

/**
 * Servlet implementation class Home
 */
// commentaire pour annuler la servlet et que ce ne soit pas lancé
//@WebServlet("/") // Mettre / quand on veut que cela commence à la racine du projet
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// remplacer par constructeur
	// etape 3 pour identifier le Bean a atteindre @EJB(lookup =
	// "corbaname:iiop:localhost:3900#java:global/library-ejb/PanierBean")
	// @EJB
	Panier panier;
	String messInfo;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Home() {
		super();

		try {
			// fait dans la classe initial contexte */ InitialContext ic = new
			// InitialContext(); // erreur de debutant cree une instance pour chaque
			// instance donc prend de la place en memoire

			panier = (Panier) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/PanierBean");
			// panier= (Panier)
			// RemoteContext.getInstance().getInitialContext().lookup("java:global/library-ear-1.0.0-SNAPSHOT/library-ejb/PanierBean");

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			messInfo = "";

			// A mettre dans une autre fonction qui sera appelée si le champ est renseigné
			String bookAdd = (String) request.getParameter("theBookAdd");
			String bookRemove = (String) request.getParameter("theBookRemove");

			if ((bookAdd != null) && (bookAdd != "")) 
				panier.addBook(bookAdd);
	

			if ((bookRemove != null) && (bookRemove != "")) 
				panier.removeBook(bookRemove);
			

			// ------

			List<String> books = panier.getBooks();
			request.setAttribute("books", books);
			// envoyer message info si besoin
			this.getServletContext().getRequestDispatcher("/jsp/index.jsp").forward(request, response);

		} catch (LibraryException e) {
			// affichage du message au client
			System.out.println(e.getMessage());
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
