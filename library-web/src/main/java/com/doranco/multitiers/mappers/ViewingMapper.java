package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.NoteVM;
import com.doranco.multitiers.vm.ViewingVM;

@Mapper
public interface ViewingMapper {
	
	
	ViewingMapper INSTANCE = Mappers.getMapper( ViewingMapper.class );
	
	@Mappings({
	    @Mapping(source = "book.id", target = "idBook"),
	    @Mapping(source = "user.id", target = "idUser"),
	})
    ViewingVM entityToVm (Viewing entity);

	List<ViewingVM> entitiesToVMs(List<Viewing> entities);
	Page<ViewingVM> entitiesPageToVMPage (Page <Viewing> entities); 

	@InheritInverseConfiguration
	Viewing vmToEntity(ViewingVM vm);

}
