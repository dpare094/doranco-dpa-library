package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.BookVM;

@Mapper
public interface BookMapper {
	
	BookMapper INSTANCE = Mappers.getMapper( BookMapper.class );

	BookVM entityToVm (Book entity);
    
    List<BookVM> entitiesToVm (List<Book> entities);   
	Page<BookVM> entitiesPageToVMPage (Page <Book> entities); 

	//  VM vers ENTITY
	//@Mapping (target="id", ignore=true) // l'id est necessaire pour la mise a jour.  
	Book vmToEntity (BookVM vm);

}
