package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.OrderLineVM;
import com.doranco.multitiers.vm.OrderVM;

@Mapper
public interface OrderLineMapper {

	OrderLineMapper INSTANCE = Mappers.getMapper( OrderLineMapper.class );

//	@Mappings({
//	@Mapping(source = "book.id", target = "idBook"), 
//	@Mapping(source = "order.id", target = "idOrder")
//	})	
	@Mapping(source = "book.id", target = "idBook")
	OrderLineVM entityToVm (OrderLine vm);

	@Mapping(target = "book.id", source = "idBook")
	OrderLine vMToEntity (OrderLineVM entity);

	List<OrderLineVM> entityToVm (List<OrderLine> entities);
	Page<OrderLineVM> entitiesPageToVMPage(Page<OrderLine> pages);

	
	
}
