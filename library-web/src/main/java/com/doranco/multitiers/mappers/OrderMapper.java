package com.doranco.multitiers.mappers;


import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.OrderVM;

@Mapper (uses=OrderLineMapper.class)
public interface OrderMapper {

	OrderMapper INSTANCE = Mappers.getMapper( OrderMapper.class );

	
    @Mapping(source = "user.id", target = "idUser")
 	OrderVM entityToVm (Order entity);

    @Mappings ({
    	@Mapping(source = "idUser", target = "user.id"),
      	@Mapping(target = "id", ignore = true)
      })
 	Order vmToEntity (OrderVM entity);

	List<OrderVM> entitiesToVMs(List<Order> entities);
//	List<Order>   vMsToEntities(List<OrderVM> vms);

	Page<OrderVM> entitiesPageToVMPage(Page<Order> pages);

//	@Mapping(source = "book.id", target = "idBook")
//	OrderLineVM entityToVm(OrderLine entity);


}
