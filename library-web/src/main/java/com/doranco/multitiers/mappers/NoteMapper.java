package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.NoteVM;


@Mapper
public interface NoteMapper {
	
	NoteMapper INSTANCE = Mappers.getMapper( NoteMapper.class );

	
	@Mappings({
	    @Mapping(source = "book.id", target = "idBook"),
	    @Mapping(source = "user.id", target = "idUser"),
		@Mapping(source = "user.userName", target = "userN"),
	    @Mapping(source = "user.firstName", target = "firstName"),
	    @Mapping(source = "user.lastName", target = "lastName")
	})
	NoteVM entityToVm (Note note);

	List<NoteVM> entitiesToVm (List<Note> entities);
	Page<NoteVM> entitiesPageToVMPage (Page <Note> entities); 


	//  VM vers ENTITY
	@Mappings({

	    @Mapping(target = "book.id", source = "idBook"),
	    @Mapping(target = "user.id", source = "idUser"),
	    @Mapping(target = "noteDate", ignore=true)
	})
	Note vmToEntity (NoteVM vm);


}
