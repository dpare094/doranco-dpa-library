package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.utils.Page;
import com.doranco.multitiers.vm.UserVM;

/**
 * 
 * @author stagiaire
 *
 */
@Mapper//(uses= {NoteMapper.class,OrderMapper.class})
public interface  UserMapper {
	
	UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );

	//  ENTITY vers VM 
	@Mappings({
		@Mapping (target="password", ignore=true),
		@Mapping (target="superAdmin", ignore=true)	
	})
	UserVM entityToVm (User entity);

	List<UserVM> entitiesToVm (List<User> entities);
	Page<UserVM> entitiesPageToVMPage (Page <User> entities); 

	//  VM vers ENTITY
	@Mappings({
		@Mapping (target="id", ignore=true),
		@Mapping (target="superAdmin", ignore=true)	
	})
	User vmToEntity (UserVM vm);

}
