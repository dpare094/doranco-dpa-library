package com.doranco.multitiers.dao.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.GenericDAO;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.utils.Page;

@RunWith(Arquillian.class)
public class DaoBookTest {

	private static Logger logger = Logger.getLogger(DaoBookTest.class);
	@EJB
	private BookDAO bookDao;

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class, "test.jar").addClasses(BookDAO.class, GenericDAO.class);

	}

	/**
	 * 
	 */
	//@Test
	@InSequence(1)
	public void testCreateBooksOk() {
		
		List<String> books = new ArrayList<String>(Arrays.asList(				
				"Et un de plus en moins",
				"Si ça continu il faut que ça cesse",
				"Vendredi 13", 
				"En attendant l'année dernière", 
				"Faut pas pousser mémé dans les orties",
				"Il fait beau et chaud, ou l'inverse", 
				"JoeBar Team", 
				"Zut le voilà", 
				"Dalt Wisney"
				));

		try {
			for (int j = 0 ; j < books.size(); j++) {
				Book book = new Book();
				if (j < 10)
					book.setIsbn("00000000" + j + "X");
				else
					book.setIsbn("0000000" + j + "X");
				
				book.setTitle(books.get(j));
				
				book = bookDao.create(book);
				logger.debug("[testCreateBook] ajout de book: " + book);

				assertNotNull(book);

			}
			
			// Ajout de BOOK
//			for (int i = 1; i < 10; i++) {
//
//				Book book = new Book();
//				if (i < 10)
//					book.setIsbn("00000000" + i + "X");
//				else
//					book.setIsbn("0000000" + i + "X");
//				book.setTitle("Le petit livre " + i);
//
//				book = bookDao.create(book);
//				logger.debug("[testCreateBook] ajout de book: " + book);
//
//				assertNotNull(book);
//			}
		} catch (Exception e) {
			logger.error("[testCreateBook] Pb de mise en place des jeux de données...", e);
			fail("[testCreateBook]  Ne devrais pas retourner d'exception");
		}
	}

	/**
	 * 
	 */
	//@Test
	@InSequence(2)
	public void testFindBookOk() {
		Book book = null;

		try {
			book = bookDao.find(Book.class, 1L);
			logger.debug("[testFindBookOk] trouve book:" + book);

		} catch (Exception e) {
			logger.error("[testFindBookOk] Pb de mise en place des jeux de données...", e);
			fail("[testFindBookOk]  erreur de recherche d'un book existant ");
		}

		assertNotNull(book);

	}

	//@Test
	@InSequence(3)
	public void testFindBookKo() {
		Book book = null;

		try {
			book = bookDao.find(Book.class, 99L);
			logger.debug("[testFindBookKo] trouve book:" + book);

		} catch (Exception e) {
			logger.error("[testFindBookKo] Pb de mise en place des jeux de données...", e);
			fail("[testFindBookKo]  erreur de recherche d'un book non existant");
		}
		assertNull(book);

	}

	/**
	 * 
	 */
	//@Test
	@InSequence(4)
	public void testUpdateBook() {
		Book book = null;

		try {

			book = bookDao.find(Book.class, 1L);
			book.setIsbn("111111111X");
			book.setTitle("Le nouveau petit livre ");

			book = bookDao.update(book);
			logger.debug("[testUpdateBook] maj book:" + book);

		} catch (Exception e) {
			logger.error("[testUpdateBook] Pb de mise en place des jeux de données...", e);
			fail("[testUpdateBook]  erreur de maj d'un book");
		}
		assertNotNull(book);

	}

	/**
	 * 
	 */
	//@Test
	@InSequence(5)
	public void testDeleteBook() {
		Book book = null;

		try {

			book = bookDao.find(Book.class, 7L);
			book = bookDao.delete(book);
			logger.debug("[testDeleteBook] delete  book:" + book);

		} catch (Exception e) {
			logger.error("[testDeleteBook] Pb de mise en place des jeux de données...", e);
			fail("[testDeleteBook]  erreur de suppression d'un book");
		}
		assertNotNull(book);
	}

	/**
	 * 
	 */
	//@Test
	@InSequence(6)
	public void testFindBookByNameOneReturn() {

		List<Book> theBooks = null;

		try {

			theBooks = bookDao.findBooksByTitleOrIsbn("Le petit livre 2");

			if (theBooks.size() != 1)
				fail("[testFindBookByNameOneReturn] la methode ne retourne pas le nombre d'element attendu(1) - Reçu :"
						+ theBooks.size());

			for (Book book : theBooks)
				logger.debug("[testFindBookByNameOneReturn] one Return : book:" + book);

		} catch (Exception e) {
			logger.error("[testFindBookByNameOneReturn] Pb de mise en place des jeux de données...", e);
			fail("[testFindBookByNameOneReturn]  erreur de recherche d'un book by name");
		}

		assertNotNull(theBooks);

	}

	//@Test
	@InSequence(7)
	public void testFindBookByNameManyReturn() {

		List<Book> theBooks = null;

		try {

			theBooks = bookDao.findBooksByTitleOrIsbn("petit livre");

			if (theBooks.size() == 0)
				fail("[testFindBookByNameManyReturn] la methode ne retourne pas le nombre d'element attendu (>1) - Reçu :"
						+ theBooks.size());

			for (Book book : theBooks)
				logger.debug("[testFindBookByNameManyReturn] many Return : book:" + book);

		} catch (Exception e) {
			logger.error("[testFindBookByNameManyReturn] Pb de mise en place des jeux de données...", e);
			fail("[testFindBookByNameManyReturn]  erreur de recherche d'un book by name");
		}
		assertNotNull(theBooks);

	}

	//@Test
	@InSequence(8)
	public void testFindBookByNameNoReturn() {

		List<Book> theBooks = null;

		try {

			theBooks = bookDao.findBooksByTitleOrIsbn("zzzzzzzz");

			if (theBooks.size() > 0)
				fail("[testFindBookByNameNoReturn] la methode ne retourne pas le nombre d'element attendu (0) - Reçu :"
						+ theBooks.size());

		} catch (Exception e) {
			logger.error("[testFindBookByNameNoReturn] Pb de mise en place des jeux de données...", e);
			fail("[testFindBookByNameNoReturn]  erreur de recherche d'un book by name");
		}
		assertNotNull(theBooks);

	}

	/**
	 * 
	 */
	//@Test
	@InSequence(9)
	public void testFindBookByIsbnOneReturn() {

		List<Book> theBooks = null;

		try {

			theBooks = bookDao.findBooksByTitleOrIsbn("000000003X");

			if (theBooks.size() != 1)
				fail("[testFindBookByIsbnOneReturn] la methode ne retourne pas le nombre d'element attendu(1) - Reçu :"
						+ theBooks.size());

			for (Book book : theBooks)
				logger.debug("[testFindBookByIsbnOneReturn] one Return : book:" + book);

		} catch (Exception e) {
			logger.error("[testFindBookByIsbnOneReturn] Pb de mise en place des jeux de données...", e);
			fail("[testFindBookByIsbnOneReturn]  erreur de recherche d'un book by Isbn");
		}

		assertNotNull(theBooks);

	}

	//@Test
	@InSequence(10)
	public void testFindBookByIsbnManyReturn() {

		List<Book> theBooks = null;

		try {

			theBooks = bookDao.findBooksByTitleOrIsbn("00000");

			if (theBooks.size() == 0)
				fail("[testFindBookByIsbnManyReturn] la methode ne retourne pas le nombre d'element attendu (>1) - Reçu :"
						+ theBooks.size());

			for (Book book : theBooks)
				logger.debug("[testFindBookByIsbnManyReturn] many Return : book:" + book);

		} catch (Exception e) {
			logger.error("[testFindBookByIsbnManyReturn] Pb de mise en place des jeux de données...", e);
			fail("[testFindBookByIsbnManyReturn]  erreur de recherche d'un book by name");
		}
		assertNotNull(theBooks);

	}

	/**
	 * 
	 */
	//@Test
	@InSequence(11)
	public void testFindAllBooks() {

		List<Book> theBooks = null;

		try {

			theBooks = bookDao.findAllBooks();
		} catch (Exception e) {
			logger.error("[testFindAllBooks] Pb de mise en place des jeux de données...", e);
			fail("[testFindAllBooks]  erreur de recherche des books");
		}
		assertNotNull(theBooks);
	}

	/**
	 * 
	 */
	//@Test
	@InSequence(12)
	public void testFindAllBooksByPage() {

		Page<Book> theBooks = null;

		try {
			theBooks = bookDao.findBooksByPage(5, 0);
			
		} catch (Exception e) {
			logger.error("[testFindAllBooksByPage] Pb de mise en place des jeux de données...", e);
			fail("[testFindAllBooksByPage]  erreur de recherche des books pat page");
		}
		assertNotNull(theBooks);

	}

	/**
	 * 
	 */
	//@Test
	@InSequence(13)
	public void findBooksByTitleOrIsbnPage() {

		Page<Book> theBooks = null;

		try {

			theBooks = bookDao.findBooksByTitleOrIsbn("00000", 5, 0);

		} catch (Exception e) {
			logger.error("[findBooksByTitleOrIsbnPage] Pb de mise en place des jeux de données...", e);
			fail("[findBooksByTitleOrIsbnPage]  erreur de recherche d'un book by name");
		}
		assertNotNull(theBooks);

	}
	
	
	//@Test
	@InSequence(14)
	public void testUpdatePrefaceAllBooks() {

		List<Book> theBooks = null;

		try {

			theBooks = bookDao.findAllBooks();			
			for (Book book : theBooks) {				
				book.setPreface("Une petite intro pour le livre '"+book.getTitle()+"' d'un auteur non moins célèbre. DPA");
				bookDao.update(book);
			}
			
		} catch (Exception e) {
			logger.error("[testFindAllBooks] Pb de mise en place des jeux de données...", e);
			fail("[testFindAllBooks]  erreur de recherche des books");
		}
		assertNotNull(theBooks);
	}

	
	//@Test
	@InSequence(15)
	public void testAverageNote() {

		List<Book> theBooks = null;

		try {

			theBooks = bookDao.findAllBooks();		
			for (Book book : theBooks) {
				logger.debug ("[testAverageNote] ---- Book:"+ book.getId()+" -- moyenne : "+ book.getAverageNote() ) ;
				
			}
			
		} catch (Exception e) {
			logger.error("[testAverageNote] Pb de mise en place des jeux de données...", e);
			fail("[testAverageNote]  erreur de recherche des books");
		}
	}

	@Test
	@InSequence(16)
	public void testGetBookNotes() {

	long book = 16; 
		try {
			
			List<Note> notes = null; 
			
			notes= bookDao.findBookNotes(book);		
			for (Note note : notes) {
				logger.debug ("[testGetBookNotes] ---- Book :"+ book+" -- moyenne : "+ note) ;			
			}
			
		} catch (Exception e) {
			logger.error("[testGetBookNotes] Pb de mise en place des jeux de données...", e);
			fail("[testGetBookNotes]  erreur de recherche des books");
		}
	}

	
	
}
