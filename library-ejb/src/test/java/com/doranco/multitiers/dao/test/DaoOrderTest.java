package com.doranco.multitiers.dao.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.GenericDAO;
import com.doranco.multitiers.dao.OrderDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.utils.Page;

@RunWith(Arquillian.class)
public class DaoOrderTest {

	private static Logger logger = Logger.getLogger(DaoOrderTest.class);
	@EJB
	OrderDAO orderDao;
	@EJB
	UserDAO userDao;

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class, "test.jar").addClasses(OrderDAO.class, UserDAO.class,
				GenericDAO.class);

	}

	@Test
	@InSequence(1)
	public void testCreateOrderOk() {

		User user;
		try {

			for (long i = 1; i < 3; i++) {

				user = userDao.findUserById(i);
				logger.debug("[testCreateOrderOk] user trouve : " + user);
				
				Order order = new Order();
				order.setUser(user);

				logger.debug("[testCreateOrderOk] l'order : " + order);

				orderDao.create(order);
				logger.debug("[testCreateOrderOk] order créé: " + order);

				assertNotNull(order);

			}

		} catch (

		Exception e) {
			logger.error("[testCreateOrderOk] Pb de mise en place des jeux de données...", e);
			fail("[testCreateOrderOk]  Ne devrais pas retourner d'exception");
		}

	}
	
	@Test
	@InSequence(2)
	public void testfindOrdersByPage() {

		Page<Order> orders = null;

		try {
			orders = orderDao.findOrdersByPage(5,0);
			assertNotNull(orders);
			
		} catch (Exception e) {
			logger.error("[testfindOrdersByPage] Pb de mise en place des jeux de données...", e);
			fail("[testfindOrdersByPage]  Ne devrais pas retourner d'exception");
		}
	}
	
}
