package com.doranco.multitiers.dao.test;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.GenericDAO;
import com.doranco.multitiers.dao.NoteDAO;
import com.doranco.multitiers.dao.OrderDAO;
import com.doranco.multitiers.dao.OrderLineDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.dao.ViewingDAO;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.entity.Viewing;

@RunWith(Arquillian.class)
public class DaoTest {

	private static Logger logger = Logger.getLogger(DaoTest.class);
	@EJB
	private BookDAO bookDao;
	@EJB
	private UserDAO userDao;
	@EJB
	private NoteDAO noteDao;
	@EJB
	private ViewingDAO viewDao;
	@EJB
	private OrderDAO orderDao;
	@EJB
	private OrderLineDAO orderLineDao;

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class, "test.jar") // on donne un nom jar : test.jar
				.addClasses(BookDAO.class, UserDAO.class, NoteDAO.class, GenericDAO.class, ViewingDAO.class,
						OrderDAO.class, OrderLineDAO.class);
	}

	//@Test
	@InSequence(1)
	public void testInsertUser() {
		try {
			logger.debug("[testInsertUser] Debut ......................");

			// Ajout de USER
			for (int i = 1; i < 5; i++) {

				User user = new User();
				user.setUserName("username" + i);
				user.setPassword("password" + i);
				user.setFirstName("Nom " + i);
				user.setLastName("Prenom " + i);

				userDao.create(user);
				logger.debug("[testInsertUser] ajout de user : " + user);
			}
		} catch (Exception e) {
			logger.error("[testInsertUser] Pb de mise en place des jeux de données...", e);
		}
	}

	//@Test
	@InSequence(2)
	public void testInsertBook() {
		try {
			logger.debug("[testInsertBook] Debut ......................");

			// Ajout de BOOK
			for (int i = 1; i < 6; i++) {

				Book book = new Book();
				if (i<10)
					book.setIsbn("00000000" + i+"X");
				else 
					book.setIsbn("0000000" + i+"X");
				book.setTitle("Le petit livre " + i);

				bookDao.create(book);
				logger.debug("[testInsertDB] ajout de book: " + book);
			}
		} catch (Exception e) {
			logger.error("[testInsertBook] Pb de mise en place des jeux de données...", e);
		}
	}

	//@Test
	@InSequence(3)
	public void testFindAllUser() {
		try {
			logger.debug("[testFindAllUser] Debut ......................");
			// Ajout de NOTE
			List<User> lesUsers;
			lesUsers = userDao.findAll(User.class);

			for (Object user : lesUsers)
				logger.debug("[testFindAllUser] :" + user);

		} catch (Exception e) {
			logger.error("[testFindAllUser] Pb de mise en place des jeux de données...", e);
		}

	}

	@Test
	@InSequence(4)
	public void testInsertNote() {
		try {
			logger.debug("[testInsertNote] Debut ......................");
			// Ajout de NOTE
			Book leBook;
			User leUser;

			for (long i = 1; i < 3; i++) {

				leUser = userDao.find(User.class, i);
				logger.debug("[testInsertNote] trouve user:" + leUser);
				leBook = bookDao.find(Book.class, 1 + i);
				logger.debug("[testInsertNote] trouve book:" + leBook);

				Note note = new Note();
				note.setUser(leUser);
				note.setBook(leBook);
				note.setNoteValue(10);
				note.setNoteDate(new Date());

				note.setNoteComment("le commentaire...");

				logger.debug("[testInsertNote] avant maj note: " + note);

//			noteDao.create(note);
				noteDao.update(note);
				logger.debug("[testInsertNote] apres maj note");
			}
		} catch (Exception e) {
			logger.error("[testInsertNote] Pb de mise en place des jeux de données...", e);
		}
	}

	// @Test
	@InSequence(5)
	public void testFindNote() {
		try {
			logger.debug("[testFindNote] Debut ......................");

			Note laNote;
			Book leBook;
			User leUser;

			leUser = userDao.find(User.class, 1L);
			logger.debug("[testFindNote] trouve user:" + leUser);
			leBook = bookDao.find(Book.class, 4L);
			logger.debug("[testFindNote] trouve book:" + leBook);

			laNote = noteDao.findNote(leUser, leBook);
			logger.debug("[testFindNote] trouve note :" + laNote);

			// laNote = noteDao.find(Note.class, 1L); // ne peut pas fonctionner car la clé
			// est double ....
		} catch (Exception e) {
			logger.error("[testFindNote] Pb de mise en place des jeux de données...", e);
		}
	}

	//@Test
	@InSequence(6)
	public void testInsertViewing() {
		try {
			logger.debug("[testInsertViewing] Debut ......................");
			Book leBook;
			User leUser;
			for (long i = 1; i < 3; i++) {
				leUser = userDao.find(User.class, i);
				logger.debug("[testInsertViewing] trouve user:" + leUser);
				leBook = bookDao.find(Book.class, 1 + i);
				logger.debug("[testInsertViewing] trouve book:" + leBook);

				Viewing view = new Viewing();
				view.setUser(leUser);
				view.setBook(leBook);
				view.setDuration(110);
				view.setStartDate(new Date());

				logger.debug("[testInsertViewing] avant maj view: " + view);

				// viewDao.create(view);
				viewDao.update(view);
				logger.debug("[testInsertViewing] apres maj view");
			}
		} catch (Exception e) {
			logger.error("[testInsertViewing] Pb de mise en place des jeux de données...", e);
		}
	}

	//@Test
	@InSequence(7)
	public void testInsertOrderAndOrderLine() {
		try {

			logger.debug("[testInsertOrderAndOrderLine] Debut ......................");
			User theUser;
			Book theBook;
			Order theOrder;
			OrderLine theOrderLine;

			for (long j = 1; j < 3; j++) {
				// Creation de la commande
				theOrder = new Order();
				// recup utilisateur 2
				theUser = userDao.find(User.class, j);

				// on place les champs et on ajoute
				theOrder.setUser(theUser);
				theOrder = orderDao.create(theOrder);

				// creation des lignes de commande
				for (long i = 1; i < 3; i++) {

					theOrderLine = new OrderLine();

					theBook = bookDao.find(Book.class, 2 + i);
					theOrderLine.setBook(theBook);
					theOrderLine.setOrder(theOrder);

					orderLineDao.update(theOrderLine);
				}
			}
		} catch (Exception e) {
			logger.error("[testInsertOrderAndOrderLine] Pb de mise en place des jeux de données...", e);
		}
	}

}
