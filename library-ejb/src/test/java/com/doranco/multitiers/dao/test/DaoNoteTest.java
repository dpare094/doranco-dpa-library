package com.doranco.multitiers.dao.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.GenericDAO;
import com.doranco.multitiers.dao.NoteDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.User;

@RunWith(Arquillian.class)
public class DaoNoteTest {

	private static Logger logger = Logger.getLogger(DaoNoteTest.class);
	@EJB
	NoteDAO noteDao;
	@EJB
	UserDAO userDao;
	@EJB
	BookDAO bookDao;

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class, "test.jar").addClasses(NoteDAO.class, UserDAO.class, BookDAO.class,
				GenericDAO.class);

	}

	@Test
	@InSequence(1)
	public void testCreateNotesOk() {

		User user;
		Book book;
		try {

			for (long i = 2; i < 4; i++) {

				user = userDao.findUserById(i );
				logger.debug("[testCreateNotesOk] user trouve : " + user);

				for (long j = 1; j < 3; j++) {
					book = bookDao.findBookById( j);
					logger.debug("[testCreateNotesOk] livre trouve : " + book);

					Note note = new Note();
					note.setUser(user);
					note.setBook(book);
					note.setNoteValue((int) (i + j));
					note.setNoteDate(new Date());
					note.setNoteComment("le commentaire du user:" + (i + 1) + " sur le livre:" + j);
					
					logger.debug("[testCreateNotesOk] la note : " + note);

					noteDao.create(note);
					//noteDao.update(note);
					logger.debug("[testCreateNotesOk] note créé: " + note);

					assertNotNull(note);
				}

			}

		} catch (Exception e) {
			logger.error("[testCreateNotesOk] Pb de mise en place des jeux de données...", e);
			fail("[testCreateNotesOk]  Ne devrais pas retourner d'exception");
		}

	}

	@Test
	@InSequence(2)
	public void testFindAllNotes() {

		List<Note> notes = null;

		try {
			notes = noteDao.findAllNotes();
			assertNotNull(notes);

		} catch (Exception e) {
			logger.error("[testFindAllNotes] Pb de mise en place des jeux de données...", e);
			fail("[testFindAllNotes]  Ne devrais pas retourner d'exception");
		}

	}

	@Test
	@InSequence(3)
	public void testFindNote() {

		Note note = null;

		try {
			User user = userDao.findUserById(1L);
			Book book = bookDao.findBookById(1L);

			note = noteDao.findNote(user, book);
			assertNotNull(note);

		} catch (Exception e) {
			logger.error("[testFindAllNotes] Pb de mise en place des jeux de données...", e);
			fail("[testFindAllNotes]  Ne devrais pas retourner d'exception");
		}

	}

	@Test
	@InSequence(4)
	public void testFindNoteById() {

		Note note = null;

		try {
	
			note = noteDao.findNoteById(1L,  1L);
			assertNotNull(note);

		} catch (Exception e) {
			logger.error("[testFindAllNotes] Pb de mise en place des jeux de données...", e);
			fail("[testFindAllNotes]  Ne devrais pas retourner d'exception");
		}

	}

	@Test
	@InSequence(5)
	public void testUpdateNoteOk() {

		Note note = null;

		try {
			note = noteDao.findNoteById(1L,  1L);

			note.setNoteComment("note modifiée....");
			noteDao.update(note);
			
			assertNotNull(note);

		} catch (Exception e) {
			logger.error("[testFindAllNotes] Pb de mise en place des jeux de données...", e);
			fail("[testFindAllNotes]  Ne devrais pas retourner d'exception");
		}

	}

	@Test
	@InSequence(6)
	public void testDeleteNoteOk() {

		Note note = null;

		try {
			note = noteDao.findNoteById(1L,  2L);
			noteDao.delete(note);
			
			assertNotNull(note);

		} catch (Exception e) {
			logger.error("[testFindAllNotes] Pb de mise en place des jeux de données...", e);
			fail("[testFindAllNotes]  Ne devrais pas retourner d'exception");
		}

	}


	
}
