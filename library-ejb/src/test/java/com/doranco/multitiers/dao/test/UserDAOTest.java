package com.doranco.multitiers.dao.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import javax.ejb.EJB;
import javax.ejb.EJBException;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.GenericDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.utils.Page;

@RunWith(Arquillian.class)
public class UserDAOTest {

	private static Logger logger = Logger.getLogger(UserDAOTest.class);

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class, "test.jar").addClasses(UserDAO.class, GenericDAO.class);
	}

	@EJB
	private UserDAO userDao;

	@Test
	@InSequence(1)
	public void testUserCreateAdminWithoutException() {

		User user = new User();
		user.setUserName("Super");
		user.setPassword("superAdmin");
		user.setFirstName("admin");
		user.setLastName("admin");
		user.setAdmin(true);
//		user.setSuperAdmin(true); plus de setter car on le positionne à la main

		try {

			userDao.create(user);
			assertNotNull(user);

		} catch (Exception e) {
			logger.error("[testUserCreateWithoutException] [ERROR] Exception qui ne devrait pas arriver", e);
			fail("[testUserCreateWithoutException]  Ne devrais pas retourner d'exception");
		}

	}

	//@Test
	@InSequence(2)
	public void testUserCreateWithoutException() {

		for (int i = 1; i < 4; i++) {
			User user = new User();
			user.setUserName("Moi"+i);
			user.setPassword("passMoi"+i);
			user.setFirstName("MonNom "+i);
			user.setLastName("MonPrenom "+i);

			try {

				userDao.create(user);
				assertNotNull(user);

			} catch (Exception e) {
				logger.error("[testUserCreateWithoutException] [ERROR] Exception qui ne devrait pas arriver", e);
				fail("[testUserCreateWithoutException]  Ne devrais pas retourner d'exception");
			}
		}

	}

	//@Test
	@InSequence(3)
	public void testUserFindGoodConnection() {

		try {

			User user = userDao.findUserByConnection("Moi", "passMoi");
			assertNotNull(user);

		} catch (Exception e) {
			logger.error("[testUserFindGoodConnection] [ERROR] Exception qui ne devrait pas arriver", e);
			fail("[testUserFindGoodConnection]  Ne devrais pas retourner d'exception");

		}

	}

	//@Test
	@InSequence(4)
	public void testUserFindWithoutGoodUserName() {

		User user = null;

		try {
			user = (User) userDao.findUserByConnection("pas_bon", "passMoi");
		} catch (EJBException e) {
		}
		assertNull(user);
	}

	//@Test
	@InSequence(5)
	public void testUserFindWithoutGoodPassword() {

		User user = null;

		try {
			user = (User) userDao.findUserByConnection("Moi", "pas_bon");
			// ici on devrait catch NoResultException, mais l'exception est envoyé dans le
			// conteneur
			// on doit donc tester EJBException
		} catch (EJBException e) {
		}
		assertNull(user);
	}
	

	//@Test
	@InSequence(6)
	public void testFindUserNotes()  {

		Page<Note>  userNote = null;

		try {
			userNote =  userDao.findUserNotesByPage(2, 5, 0);
						
		} catch (Exception e) {
			logger.error("[testFindUserNotes] [ERROR] Exception qui ne devrait pas arriver", e);
			fail("[testFindUserNotes]  Ne devrais pas retourner d'exception");

		}
		assertNotNull(userNote);
	}


	@Test
	@InSequence(7)
	public void testFindUserByUserName() {

		User user;
		try {
			user = (User) userDao.findUserByUserName("nouveau");

			logger.debug("[testFindUserByUserName] user trouve : "+ user);
			
		} catch (Exception e) {
			logger.error("[testFindUserByUserName] [ERROR] Exception qui ne devrait pas arriver", e);
			fail("[testFindUserByUserName]  Ne devrais pas retourner d'exception");

		}
		
	}

	
	

	// @Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	// @Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	// @Test
	// @InSequence(4)
	public void testFindUser() {

		User user = null;
		try {
			user = (User) userDao.find(User.class, 1L);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertNotNull(user);

		// logger.debug("[testFindUser]user 1 : "+ user);

	}

	// @Test
	public void testFindAllUser() {
		fail("Not yet implemented");
	}

}
