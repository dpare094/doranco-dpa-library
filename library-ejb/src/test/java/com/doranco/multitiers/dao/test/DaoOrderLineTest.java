package com.doranco.multitiers.dao.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.GenericDAO;
import com.doranco.multitiers.dao.OrderDAO;
import com.doranco.multitiers.dao.OrderLineDAO;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;

@RunWith(Arquillian.class)
public class DaoOrderLineTest {

	private static Logger logger = Logger.getLogger(DaoOrderLineTest.class);
	@EJB
	OrderLineDAO orderLineDao;
	@EJB
	OrderDAO orderDao;
	@EJB
	BookDAO bookDao;

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class, "test.jar").addClasses(OrderLineDAO.class, OrderDAO.class, BookDAO.class,
				GenericDAO.class);

	}

	@Test
	@InSequence(1)
	public void testCreateOrderLinesOk() {

		Order order;
		Book book;
		try {

			for (long i = 1; i < 3; i++) {

				order = orderDao.find(Order.class, i );
				logger.debug("[testCreateOrderLinesOk] order trouve : " + order);

				for (long j = 1; j < 3; j++) {
					book = bookDao.findBookById( j+i );
					logger.debug("[testCreateOrderLinesOk] livre trouve : " + book);

					OrderLine orderLine = new OrderLine();
					orderLine.setOrder(order);
					orderLine.setBook(book);
					
					logger.debug("[testCreateNotesOk] l'orderline : " + orderLine);

					orderLineDao.create(orderLine);
					logger.debug("[testCreateNotesOk] l'orderline créé: " + orderLine);

					assertNotNull(orderLine);
				}

			}

		} catch (Exception e) {
			logger.error("[testCreateOrderLinesOk] Pb de mise en place des jeux de données...", e);
			fail("[testCreateOrderLinesOk]  Ne devrais pas retourner d'exception");
		}

	}
	
	
}
