package com.doranco.multitiers.ejb.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.ViewingDAO;
import com.doranco.multitiers.ejb.interfaces.IBook;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.EntityNotFoundException;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.ViewingNotFoundException;
import com.doranco.multitiers.utils.Page;

@Stateless
public class BookBean implements IBook {

	private static Logger logger = Logger.getLogger(UserBean.class);
	@EJB
	BookDAO bookDao;
	@EJB
	ViewingDAO viewingDao;

	/**
	 * CUD
	 * 
	 */
	@Override
	public Book createBook(Book book) throws LibraryException {

		try {
			book = bookDao.create(book);

		} catch (Exception e) {
			logger.error("[ERROR]Problème lors de la creation d'un livre", e);
			throw new LibraryException(e);
		}

		return book;
	}

	@Override
	public Book updateBook(Book book) throws LibraryException {
		try {
			book = bookDao.update(book);

		} catch (Exception e) {
			logger.error("[ERROR]Problème lors de la maj d'un livre", e);
			throw new LibraryException(e);
		}

		return book;
	}

	@Override
	public Book deleteBook(long id) throws LibraryException {
		Book book = null;
		try {
			book = bookDao.findBookById(id);
			book = bookDao.delete(book);

		} catch (Exception e) {
			logger.error("[ERROR]Problème lors de la suppression d'un livre", e);
			throw new LibraryException(e);
		}

		return book;
	}

	/**
	 * 
	 */
	@Override
	public List<Book> findAllBooks() throws LibraryException {
		List<Book> lesBooks = null;

		try {
			lesBooks = bookDao.findAllBooks();

		} catch (Exception e) {
			logger.error("[ERROR]Aucun book trouvé", e);
			throw new LibraryException(e);
		}
		return lesBooks;

	}

	/**
	 * 
	 */
	@Override
	public Page<Book> findBooksByPage(int pageSize, int pageNumber) throws LibraryException {

		Page<Book> theBooksByPage = null;

		try {

			theBooksByPage = bookDao.findBooksByPage(pageSize, pageNumber);

		} catch (Exception e) {
			logger.error("[ERROR]Aucun books trouvé", e);
			throw new LibraryException(e);
		}
		if (theBooksByPage == null)
			throw new EntityNotFoundException();

		return theBooksByPage;
	}

	/**
	 * @throws Exception
	 * 
	 *                   on place l'idUser de type Long --- et non du type primitif
	 *                   - pour avoir un null
	 */
	@Override
	public Book findBookById(long idBook, Long idUser) throws LibraryException {

		Book book = null;
		Viewing viewing = null;

			//
		// Si l'idUser n'est pas renseigné alors on cherche juste le livre
		//

		if (idUser == null) {
			try {
				book = bookDao.findBookById(idBook);

			} catch (Exception e) {
				logger.error("[ERROR]Aucun livre trouvé par Id", e);
				throw new LibraryException(e);
			}
			if (book == null)
				throw new EntityNotFoundException();

		}

		//
		// recherche de la demande de consultation
		//

		if (idUser != null) {
			try {
				viewing = viewingDao.findViewingsByIds(idUser, idBook);

	
			} catch (EJBTransactionRolledbackException nre) {
				ViewingNotFoundException une = new ViewingNotFoundException(nre);

				logger.error("[ERROR]   demande de consultation trouvée", une);
				throw une; // transfert l'erreur
			}

			// Si on trouve qquechose
			if (viewing != null) {
				// alors on test pour voir si la durée n'est pas dépassée
				if ((viewing.getDuration() * 3600 * 1000) < ((new Date().getTime())- viewing.getStartDate().getTime())) {
					ViewingNotFoundException une = new ViewingNotFoundException();
					logger.error("[ERROR] date de lecture depassée", une);
					throw une;

				}
				// on retourne le book si tout est ok
				book = viewing.getBook();
				return book;
				
			} else {
				logger.error("[ERROR]Aucune demande de vue trouvée");
				throw new ViewingNotFoundException();
			}
		}

		return book;
	}

	/**
	 * 
	 */
	@Override
	public List<Book> findBooksByTitleOrIsbn(String bookNameIsbn) throws LibraryException {

		List<Book> book = null;

		try {
			book = bookDao.findBooksByTitleOrIsbn(bookNameIsbn);

		} catch (Exception e) {
			logger.error("[ERROR]Aucun livre trouvé par nom ou Isbn", e);
			throw new LibraryException(e);
		}

		if (book == null)
			throw new EntityNotFoundException();

		return book;
	}


	/**
	 * 
	 */
	@Override
	public Page<Book> findBooksByTitleOrIsbn(String bookNameIsbn, int pageSize, int pageNumber)
			throws LibraryException {
		Page<Book> book = null;

		try {
			book = bookDao.findBooksByTitleOrIsbn(bookNameIsbn, pageSize, pageNumber);

		} catch (Exception e) {
			logger.error("[ERROR]Aucun livre trouvé par nom ou Isbn", e);
			throw new LibraryException(e);
		}
		if (book == null)
			throw new EntityNotFoundException();

		return book;
	}

	@Override
	public Page<Note> findBookNotesByPage(long bookId, int pageSize, int pageNumber) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Override
	public Page<Viewing> findBookViewingsByPage(long bookId, int pageSize, int pageNumber) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public long getAverageNoteBook(long bookId) throws LibraryException {
		long average = 0;
		try {
			average =  bookDao.getAverageNoteBook(bookId);
		} catch (Exception e) {
			logger.error("[ERROR]Problème de recuération de la moyenne des notes", e);
			throw new LibraryException(e);
		}
		
		return average;
	}
	

	public List<Note> findBookNotes(long bookId) throws LibraryException {
		List<Note> notes;
		
	
		try {
			notes =  bookDao.findBookNotes(bookId);
		} catch (Exception e) {
			logger.error("[ERROR]Problème de recuération des notes", e);
			throw new LibraryException(e);
		}
		
		 return notes ;
	}

	//
	public Double getAverageNote(long bookId) throws LibraryException {
		List<Note> notes;
		
		Double mean = 0d;
		long sum=0;
		
		try {
			notes =  bookDao.findBookNotes(bookId);
			for (Note note : notes) {
				sum += note.getNoteValue(); 					
			}
			if (notes.size() > 0) mean =  (double) (sum/notes.size()); 
			
		} catch (Exception e) {
			logger.error("[ERROR]Problème calcul de la moyenne des notes", e);
			throw new LibraryException(e);
		}
		
		 return mean ;
	}


}
