package com.doranco.multitiers.ejb.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.OrderDAO;
import com.doranco.multitiers.dao.OrderLineDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.ejb.interfaces.IOrder;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.EntityNotFoundException;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;

@Stateless
public class OrderBean implements IOrder {

	private static Logger logger = Logger.getLogger(OrderBean.class);
	@EJB
	OrderDAO orderDao;
	@EJB
	OrderLineDAO orderLineDao;
	@EJB
	UserDAO userDao;
	@EJB
	BookDAO bookDao;

	/**
	 * 
	 */
	@Override
	public Order createOrder(Order order) throws LibraryException {
		Order orderAdd;

		try {

			// recupération du user // test de l'existence du user dans la base
			User user = userDao.findUserById(order.getUser().getId());
			order.setUser(user);
			// Creation de la commande
			orderAdd = orderDao.create(order);

			// boucle sur les books
			for (int i = 0; i < order.getOrderLines().size(); i++) {
				// for (OrderLine line : order.getOrderLines()) {
				OrderLine line = new OrderLine();

				// recuperation du book // test de l'existence du book dans la base
				line = order.getOrderLines().get(i);
				Book book = bookDao.findBookById(line.getBook().getId());

				line.setOrder(orderAdd);
				line.setBook(book);

				// insertion de l'order line
				line = orderLineDao.create(line);

			}

			orderAdd.setOrderLines(order.getOrderLines());

		} catch (Exception e) {
			logger.error("[ERROR]Problème lors de la creation d'un Order", e);
			throw new LibraryException(e);
		}
		return orderAdd;

	}

	/**
	 * 
	 */
	@Override
	public Page<Order> findOrdersByPage(int pageSize, int pageNumber) throws LibraryException {
		Page<Order> theOrders = null;
		List<OrderLine> orderLines;

		try {
			theOrders = orderDao.findOrdersByPage(pageSize, pageNumber);

		} catch (Exception e) {
			logger.error("[ERROR]Aucune commande trouvée", e);
			throw new LibraryException(e);
		}
		if (theOrders == null)
			throw new EntityNotFoundException();

		// on recherche les lignes des commandes
		for (Order order : theOrders.getContent()) 
			order.setOrderLines(orderLineDao.findOrderLinesOrder(order));
		

		return theOrders;
	}
	
	/**
	 * 
	 */
	@Override
	public Page<OrderLine> findOrderLinesOrderByPage(long id, int pageSize, int pageNumber) throws LibraryException {
		Page<OrderLine> theOrders = null;

		try {
			theOrders = orderDao.findOrderLinesOrderByPage (id, pageSize, pageNumber);
			
		} catch (Exception e) {
			logger.error("[ERROR]Aucune ligne commande trouvée", e);
			throw new LibraryException(e);
		}
		
		return theOrders;
	}


}
