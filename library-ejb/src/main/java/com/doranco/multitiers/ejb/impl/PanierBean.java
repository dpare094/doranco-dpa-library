package com.doranco.multitiers.ejb.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;

import com.doranco.multitiers.ejb.interfaces.Panier;
import com.doranco.multitiers.exceptions.LibraryException;

@Stateless
public class PanierBean implements Panier {

	List<String> books = new ArrayList<String>(Arrays.asList("Et un de plus en moins",
			"Si ça continu il faut que ça cesse", "Vendredi 13", "En attendant l'année dernière"));
	// Arrays.asList permet d'ajouter dans l'array directement sans faire de add
	

	

	@Override
	public void addBook(String book) throws LibraryException {

		try {
			if ((book != null) && (book != "")) {
				// ajouter test existence
				if (!this.isBookExist(book)) 
					// on ajoute le livre
					this.books.add(book);
				//else 
					//messageInfo= "Le livre: '"+book +"' existe déjà.";
				// exception
			}

		} catch (Exception e) {
			// mettre en place un logueur : loguer la stack trace dans le fichier de logs
			e.printStackTrace();
			throw new LibraryException("[ERROR] Problème survenue lors de l'ajout d'un livre.");
		}
	}

	@Override
	public void removeBook(String book) throws LibraryException {

		try {
			if ((book != null) && (book != "")) {
				// ajouter test existence
				if (this.isBookExist(book))
					// On supprime le livre
					this.books.remove(book);
				//else 
				//	messageInfo= "Le livre: '"+book +"' n'existe pas."; // A faire par exception
				// exception
			}
		} catch (Exception e) {
			// mettre en place un logueur : loguer la stack trace dans le fichier de logs
			e.printStackTrace();
			throw new LibraryException("[ERROR] Problème survenue lors de la suppression d'un livre.");
		}

	}

	@Override
	public List<String> getBooks() throws LibraryException {

		List<String> results = null;
		try {
			// recupère les livres de la base de données
			results = books;

		} catch (Exception e) {
			// mettre en place un logueur : loguer la stack trace dans le fichier de logs
			e.printStackTrace();
			throw new LibraryException("[ERROR] Problème survenue lors de la récupération des livres.");
		}
		return results;
	}

	@Override
	public boolean isBookExist(String book) throws LibraryException {

		boolean trouve = false;

		try {
			for (int i = 0; i < this.books.size() && trouve == false; i++)
				// si on trouve le livre alors l'indiquer
				if (this.books.get(i).equals(book))
					trouve = true;

		} catch (Exception e) {
			// mettre en place un logueur : loguer la stack trace dans le fichier de logs
			e.printStackTrace();
			throw new LibraryException("[ERROR] Problème survenue lors de la recherche de l'existence d'un livre.");
		}

		return trouve;
	}

}
