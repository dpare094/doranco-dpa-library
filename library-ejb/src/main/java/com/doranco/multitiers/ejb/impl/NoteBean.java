package com.doranco.multitiers.ejb.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.NoteDAO;
import com.doranco.multitiers.ejb.interfaces.INote;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.EntityNotFoundException;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;

@Stateless
public class NoteBean implements INote {

	private static Logger logger = Logger.getLogger(NoteBean.class);
	@EJB
	NoteDAO noteDao;
//	@EJB
//	UserDAO userDao;
//	@EJB
//	BookDAO bookDao;

	/**
	 * 
	 */
	@Override
	public Note createNote(Note note) throws LibraryException {

		try {

//			Book book = bookDao.find(Book.class, note.getBook().getId());
//			note.setBook(book);
//			User user = userDao.find(User.class, note.getUser().getId());
//			note.setUser(user);
			note.setNoteDate(new Date());
			note = noteDao.create(note);

		} catch (Exception e) {
			logger.error("Probl�me lors de la creation d'une note", e);
			throw new LibraryException(e);
		}
		return note;
	}

	@Override
	public Note updateNote(Note note) throws LibraryException {

		try {
			note = noteDao.update(note);

		} catch (Exception e) {
			logger.error("Probl�me lors de la modification d'une note", e);
			throw new LibraryException(e);
		}
		return note;
	}

	@Override
	public void deleteNote(long idUser, long idBook) throws LibraryException {

//		User user = null;
//		Book book = null;
		Note note = null;

		try {
			note = noteDao.findNoteById(idUser, idBook);
			note = noteDao.delete(note);

//			user = userDao.findUserById(idUser);
//			note.setUser(user);
//
//			book = bookDao.findBookById(idBook);
//			note.setBook(book);
//
//			logger.debug("----------------> user :" + user);
//			logger.debug("----------------> book :" + book);
//			if ((user != null) && (book != null)) {
//				note = noteDao.findNote(user, book);
//				logger.debug("----------------> note :" + note);
//			}
//			note = noteDao.delete(note);

		} catch (Exception e) {
			logger.error("Probl�me lors de la suppression d'une note", e);
			throw new LibraryException(e);
		}

	}

	/**
	 * 
	 */
	@Override
	public List<Note> findAllNotes() throws LibraryException {
		List<Note> lesNotes = null;

		try {
			lesNotes = noteDao.findAllNotes();

		} catch (Exception e) {
			logger.error("Aucune Notes trouv�es", e);
			throw new LibraryException(e);
		}
		if (lesNotes == null)
			throw new EntityNotFoundException();
		return lesNotes;

	}

	/**
	 * 
	 */
	@Override
	public Page<Note> findNotesByPage(int pageSize, int pageNumber) throws LibraryException {

		Page<Note> theNotesByPage = null;

		try {
			theNotesByPage = noteDao.findNotesByPage(pageSize, pageNumber);

		} catch (Exception e) {
			logger.error("Aucune notes trouv�es", e);
			throw new LibraryException(e);
		}

		if (theNotesByPage == null)
			throw new EntityNotFoundException();

		return theNotesByPage;

	}

	@Override
	public Note findNote(User user, Book book) throws LibraryException {

		Note note = null;

		try {
			note = noteDao.findNote(user, book);

		} catch (Exception e) {
			logger.error("Pas de note trouv�e", e);
			throw new LibraryException(e);
		}

		if (note == null)
			throw new EntityNotFoundException();

		return note;
	}

	@Override
	public Note findNote(long idUser, long idBook) throws LibraryException {
		Note note = null;

		try {
			note = noteDao.findNoteById(idUser, idBook);

		} catch (Exception e) {
			logger.error("Pas de note trouv�e", e);
			throw new LibraryException(e);
		}

		if (note == null)
			throw new EntityNotFoundException();

		return note;
	}

	@Override
	public List<Note> findNotes( long idBook) throws LibraryException {
		List<Note> notes = null;
		try {
			notes = noteDao.findNotesByBook( idBook);
logger.debug("--> findNotes : "+ notes + "//"+ idBook);	
		} catch (Exception e) {
			logger.error("Pas de note trouv�e pour ce livre", e);
			throw new LibraryException(e);
		}

		if (notes == null)
			throw new EntityNotFoundException();

		return notes;
	}

}
