package com.doranco.multitiers.ejb.impl;



import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.dao.ViewingDAO;
import com.doranco.multitiers.ejb.interfaces.IViewing;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.EntityNotFoundException;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.UserNotFoundException;
import com.doranco.multitiers.exceptions.ViewingNotFoundException;
import com.doranco.multitiers.utils.Page;

@Stateless
public class ViewingBean implements IViewing {

	private static Logger logger = Logger.getLogger(ViewingBean.class);
	@EJB
	ViewingDAO viewingDao;
	@EJB
	UserDAO userDao;
	@EJB
	BookDAO bookDao;

	/**
	 * 
	 */
	@Override
	public Viewing createViewing(Viewing viewing) throws LibraryException {
		try {

			Book book = bookDao.findBookById(viewing.getBook().getId());
			viewing.setBook(book);
			User user = userDao.findUserById(viewing.getUser().getId());
			viewing.setUser(user);
			viewing.setStartDate(new Date());

			viewing = viewingDao.create(viewing);

		} catch (Exception e) {
			logger.error("Problème lors de la creation d'une viewing", e);
			throw new LibraryException(e);
		}
		return viewing;
	}

	
	@Override
	public Viewing updateViewing(Viewing viewing) throws LibraryException {

		try {
			viewing.setStartDate(
					viewingDao.findViewingsByIds(viewing.getUser().getId(), viewing.getBook().getId()).getStartDate());
			viewing = viewingDao.update(viewing);

		} catch (Exception e) {
			logger.error("Problème lors de la modification d'une viewing", e);
			throw new LibraryException(e);
		}
		return viewing;
	}

	@Override
	public void deleteViewing(long idUser, long idBook) throws LibraryException {

		Viewing viewing = null;

		try {
			viewing = viewingDao.findViewingsByIds(idUser, idBook);
			viewing = viewingDao.delete(viewing);

		} catch (Exception e) {
			logger.error("Problème lors de la suppression d'une note", e);
			throw new LibraryException(e);
		}

	}

	/**
	 * 
	 */
	@Override
	public Page<Viewing> findViewingsByPage(int pageSize, int pageNumber) throws LibraryException {
		Page<Viewing> thePages = null;

		try {
			thePages = viewingDao.findViewingsByPage(pageSize, pageNumber);

		} catch (Exception e) {
			logger.error("Aucune Viewing trouvées", e);
			throw new LibraryException(e);
		}

		if (thePages == null)
			throw new EntityNotFoundException();

		return thePages;
	}

	@Override
	public Viewing findViewingsByIds(long idUser, long idBook) throws LibraryException {

		Viewing viewing;
		try {

			viewing = viewingDao.findViewingsByIds(idUser, idBook);
					
		} catch (Exception e) {
			logger.error("[ERROR]   viewing  non trouvé", e);
			throw new LibraryException(e); 

		}
		return viewing;
	}

}
