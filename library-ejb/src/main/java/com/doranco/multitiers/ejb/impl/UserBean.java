package com.doranco.multitiers.ejb.impl;

import java.io.IOException;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.LibraryConstants;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.ejb.interfaces.IUser;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.EntityNotFoundException;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.UserNotFoundException;
import com.doranco.multitiers.utils.KeyGenerator;
import com.doranco.multitiers.utils.Page;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Stateless
public class UserBean implements IUser {

	private static Logger logger = Logger.getLogger(UserBean.class);

	@EJB // retrourne une et une seule instance de UserDAO
	UserDAO userDao; // On obtient tout le contenu de UserDAO

	/**
	 * issueToken pour recupere la clé de chiffrement
	 * 
	 * @param login
	 * @param uriInfo
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 * @throws CertificateException
	 * @throws KeyStoreException
	 * @throws UnrecoverableKeyException
	 */
	private String issueToken(String login, String uriInfo, boolean setAdminClaim, boolean setSuperAdminClaim)
			throws NoSuchAlgorithmException, UnrecoverableKeyException, KeyStoreException, CertificateException,
			IOException {

		logger.debug("[issueToken] uriInfo ==> " + uriInfo);
		Key key = KeyGenerator.getINSTANCE().getKey(); // notre KeyGenerator
		logger.debug("[issueToken] key  ==> " + key);

		JwtBuilder jwtBuild = Jwts.builder().setSubject(login).setIssuer(uriInfo).setIssuedAt(new Date())
				.setExpiration(toDate(LocalDateTime.now().plusMinutes(15L)));

		List<String> roles = new ArrayList<String>();

		if (setSuperAdminClaim)
			// jwtBuild.claim(LibraryConstants.JWT_ROLE_KEY,
			// LibraryConstants.SUPER_ADMIN_ROLE);
			roles.add(LibraryConstants.SUPER_ADMIN_ROLE);
		if (setAdminClaim)
			// jwtBuild.claim(LibraryConstants.JWT_ROLE_KEY, LibraryConstants.ADMIN_ROLE);
			roles.add(LibraryConstants.ADMIN_ROLE);

		jwtBuild.claim(LibraryConstants.JWT_ROLE_KEY, roles);

		String jwtToken = jwtBuild.signWith(SignatureAlgorithm.HS512, key).compact();

		logger.debug("#### generating token for a key : " + jwtToken + " - " + key);

		return jwtToken;
	}

	/**
	 * 
	 * @param localDateTime
	 * @return
	 */
	private Date toDate(LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * connect
	 * 
	 */
	@Override
	public String connect(String userName, String password, String uriInfoPath) throws LibraryException {

		String token = null;

		try {
			User user = userDao.findUserByConnection(userName, password);
			logger.debug("[connect] ------------" + user.isAdmin() + "/" + user.isSuperAdmin());
			token = issueToken(userName, uriInfoPath, user.isAdmin(), user.isSuperAdmin());

			// TODO attraper EJBEXeption en remplacement de NoResultException car la methode
			// retourne certainement l'exception
		} catch (EJBTransactionRolledbackException nre) {
			UserNotFoundException une = new UserNotFoundException(nre);

			logger.error("[ERROR]   Utilisateur " + userName + " non trouvé", une);
			throw une; // transfert l'erreur

		} catch (Exception e) {
			logger.error("Problème lors de la connexion de l'utilisateur", e);
			throw new LibraryException(e);

		}
		return token;
	}

	/**
	 * subscribe
	 * 
	 */
	@Override
	public User subscribe(User user) throws LibraryException {

		try {
			// test si utilisateur est admin pour positionner le champ isAdmin à true sinon
			// false
			user.setAdmin(false);

			user = userDao.create(user);

		} catch (Exception e) {
			logger.error("Problème lors de l'inscription d'un utilisateur", e);
			throw new LibraryException(e);
		}

		return user;
	}

	/**
	 * idModifier = id du user connecté. Celui qui veut modifier la valeur
	 */
	@Override
	public User setAsAdmin(long id) throws LibraryException {

		User user = null;
		try {

			user = userDao.findUserById(id);
			user.setAdmin(true);

			user = userDao.update(user);

		} catch (Exception e) {
			logger.error("Problème lors de la mise à jour d'un utilisateur en Admin", e);
			throw new LibraryException(e);
		}

		return user;

	}

	/**
	 * retourne la liste de tous les elements
	 */
	@Override
	public List<User> findAllUser() throws LibraryException {

		List<User> lesUsers = null;

		try {
			lesUsers = userDao.findAllUser();

		} catch (Exception e) {
			logger.error("Aucun utilisateur trouvé", e);
			throw new LibraryException(e);
		}
		if (lesUsers == null)
			throw new EntityNotFoundException();
		return lesUsers;
	}

	/**
	 * retourne la listes des elements par page
	 */
	@Override
	public Page<User> findUsersByPage(int pageSize, int pageNumber) throws LibraryException {

		Page<User> theUsersByPage = null;

		try {

			theUsersByPage = userDao.findUsersByPage(pageSize, pageNumber);

		} catch (Exception e) {
			logger.error("Aucun utilisateurs trouvé", e);
			throw new LibraryException(e);
		}
		if (theUsersByPage == null)
			throw new EntityNotFoundException();

		return theUsersByPage;
	}

	/**
	 * 
	 * 
	 */
	@Override
	public User findUser(long id) throws LibraryException {

		User user = null;

		try {
			user = userDao.findUserById(id);

		} catch (Exception e) {
			logger.error("Aucun utilisateurs trouvé", e);
			throw new LibraryException(e);
		}

		if (user == null)
			throw new EntityNotFoundException();

		return user;
	}

	/**
	 * 
	 */
	@Override
	public Page<Note> findUserNotesByPage(long userId, int pageSize, int pageNumber) throws LibraryException {

		Page<Note> theUserNotesByPage = null;

		try {

			theUserNotesByPage = userDao.findUserNotesByPage(userId, pageSize, pageNumber);

		} catch (Exception e) {
			logger.error("Aucune notes pour l'utilisateur trouvé", e);
			throw new LibraryException(e);
		}
		if (theUserNotesByPage == null)
			throw new EntityNotFoundException();

		return theUserNotesByPage;
	}

	@Override
	public Page<Viewing> findUserViewingsByPage(long userId, int pageSize, int pageNumber) throws LibraryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Order> findUserOrdersByPage(long userId, int pageSize, int pageNumber) throws LibraryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findUserByUserName(String userName) throws LibraryException {

		User user;
		try {
			user = userDao.findUserByUserName(userName);
			
		} catch (Exception e) {
			logger.error("Aucune utilisateur trouvé pour un userName", e);
			throw new LibraryException(e);
		}
		if (user == null)
			throw new EntityNotFoundException();

		return user;
	}
	
	@Override
	public User findUserByConnect(String userName, String password) throws LibraryException {
		User user;
		try {
			user = userDao.findUserByConnection(userName, password);

		} catch (Exception e) {
			logger.error("Problème ", e);
			throw new LibraryException(e);

		}
		return user;
	}



}
