package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;

import com.doranco.multitiers.ejb.impl.NoteBean;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.utils.Page;


@Stateless
@LocalBean
public class NoteDAO extends GenericDAO <Note> {

	private static Logger logger = Logger.getLogger(NoteDAO.class);

	/**
	 * 
	 */
	public NoteDAO()  {super();}
	
//	/**
//	 * 
//	 * @param userId
//	 * @param bookId
//	 * @throws Exception
//	 */
//	public void deleteNote (long userId, long bookId )throws Exception {
//		
//		String requete = "DELETE FROM Note n WHERE n.user.id =:p_user AND n.book.id = :p_book"; 
//		em.createQuery(requete).setParameter("p_user", userId).setParameter("p_book", bookId); 
//		
//	}
	
	/**
	 * 
	 * @param userId
	 * @param bookId
	 * @return
	 * @throws Exception
	 */
	public Note findNoteById (long userId, long bookId )throws Exception {
		
		String requete = "Select n FROM Note n WHERE n.user.id =:p_user AND n.book.id = :p_book"; 
		return (Note) em.createQuery(requete).setParameter("p_user", userId).setParameter("p_book", bookId).getSingleResult(); 
		
	}	
	
	/**
	 * 
	 * @param p_user
	 * @param p_book
	 * @return
	 * @throws NoResultException
	 */
	public Note findNote(User p_user, Book p_book) throws NoResultException {

		Note theNote = null;

		String requete = "SELECT n FROM Note n WHERE n.user=:p_user AND n.book=:p_book"; 
		theNote = (Note) em.createQuery(requete)
				.setParameter("p_user", p_user).setParameter("p_book", p_book)
				.getSingleResult();
				
		return theNote;
	}

	/**
	 * 
	 * @return
	 * @throws Exception 
	 */
	public List<Note> findAllNotes() throws Exception {
		List<Note> notes;

		notes = this.findAll(Note.class);
		
		return notes;
	}
	
	/**
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<Note> findNotesByPage(int pageSize, int pageNumber) throws Exception {

		Page<Note> page;
		List<Note> pageContent;
		long nbRow;

		nbRow = this.findNbRow(Note.class); // nombre d'enregistrement de l'entité
		pageContent = this.findByPage(Note.class, pageSize, pageNumber); // les enregistrements a l'emplacement spécifié

	
		page = new Page<Note>(pageSize, pageNumber, nbRow, pageContent);

		return page;
	}

	
	/**
	 * 
	 * @param p_book
	 * @return
	 */
	public List<Note> findNotesByBook(long p_book) {
//		Note theNote = null;
		List<Note> theNote;
				
				
		String requete = "SELECT n FROM Note n WHERE  n.book.id=:p_book"; 
		theNote = (List<Note>) em.createQuery(requete)
				.setParameter("p_book", p_book)
				.getResultList();
				//.getSingleResult();
	logger.debug("[findNotesByBook] --> "+p_book +" / "+ theNote);			
		return theNote;	
	}	
	
}
