package com.doranco.multitiers.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.utils.Page;



public class GenericDAO<T> {
	
	private static Logger logger = Logger.getLogger(GenericDAO.class);

	// permet a jpa de manipuler la bd dans toutes les entités
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("library"); // permet d'acceder a un conteneur																					// sans utiliser EJB
	EntityManager em = emf.createEntityManager();
	EntityTransaction tx = em.getTransaction();

	/**
	 * Methodes générique CRUD
	 * 
	 * @param toMaj
	 * @return
	 * @throws Exception
	 */
	public T create(T toMaj) throws Exception {
	
		tx.begin();
		toMaj = em.merge(toMaj); // on fait un merge et pas un persist car on a un soucis avec notes/viewing
		//em.persist(toMaj);
		tx.commit();

		// retourne le panier
		return toMaj;

	}

	public T update(T toBeUpdated) throws Exception {

		tx.begin();
		toBeUpdated = em.merge(toBeUpdated);
		tx.commit();
		return toBeUpdated;
	}

	public T delete(T toDelete) throws Exception {

		tx.begin();
		em.remove(toDelete);
		tx.commit();

		// retourne le panier
		return toDelete;

	}

	
	/**
	 * find
	 * Retourne un enregistrement d'une entité
	 * 
	 * @param clazz
	 * @param primaryKey
	 * @return
	 * @throws Exception
	 */
	public T find(Class clazz, long primaryKey) throws Exception {
		T trouve = null;
		
		logger.debug ("FIND : avant test, je cherche :" + primaryKey); 
		trouve = (T) em.find(clazz, primaryKey); 
		//em.merge(trouve);		
		return trouve;
	}

	/**
	 * findAll
	 * Retourne tous les enregistrements d'une entité
	 * 
	 * @param clazz
	 * @return
	 * @throws Exception
	 */
	public List<T> findAll(Class clazz)  throws Exception{

		String requete = "SELECT entityObj FROM " + clazz.getName() + " entityObj ";
		return em.createQuery(requete).getResultList();
		
	}
	
	/**
	 * findNbRow
	 * retourne le nombre d'enregristrement d'une entité
	 * 
	 * @param clazz
	 * @return
	 * @throws Exception
	 */
	public long findNbRow(Class clazz)  throws Exception{

		String requete = "SELECT count(*) FROM " + clazz.getName();
		return (long)em.createQuery(requete).getSingleResult();
		
	}
	
	/**
	 * findByPage
	 * retourne un nombre d'enregistrement sous forme de page
	 * (un nombre de ligne defini)
	 * 
	 * @param clazz
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public List<T> findByPage(Class clazz, int pageSize, int pageNumber)  throws Exception{
		
		List<T> pageContent;
		
		String requete = "SELECT entityObj FROM " + clazz.getName() +" entityObj";
		Query query = em.createQuery(requete);
		
		query.setMaxResults(pageSize);
		query.setFirstResult(pageSize*pageNumber); 		
		pageContent = query.getResultList();	
			
		return pageContent;
		
	}
	


}
