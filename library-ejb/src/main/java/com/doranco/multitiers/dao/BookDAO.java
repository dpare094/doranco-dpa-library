package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.log4j.Logger;


import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.utils.Page;

@Stateless
@LocalBean
public class BookDAO extends GenericDAO<Book> {

	private static Logger logger = Logger.getLogger(BookDAO.class);

	/**
	 * 
	 */
	public BookDAO() {super();}

	
	/**
	 * 
	 * @param bookNameOrIsbn
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<Book> findBooksByTitleOrIsbn(String bookNameOrIsbn, int pageSize, int pageNumber) throws Exception {
		
		bookNameOrIsbn = "%" + bookNameOrIsbn + "%";

		String requete = "SELECT b FROM Book b WHERE b.title like :p_book or b.ISBN like :p_book";
		List<Book> pageContent = em.createQuery(requete).setParameter("p_book", bookNameOrIsbn)
									.setFirstResult(pageNumber*pageSize)
									.setMaxResults(pageSize)
									.getResultList();

		long nbRow = (long) em.createQuery("SELECT count(*) FROM Book b WHERE b.title like :p_book or b.ISBN like :p_book")
									.setParameter("p_book", bookNameOrIsbn)
									.getSingleResult();
				
		Page<Book> page = new Page<Book>(pageSize, pageNumber, nbRow, pageContent);
		return page;

	}

	
	/**
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<Book> findBooksByPage(int pageSize, int pageNumber) throws Exception {

		Page<Book> page;
		List<Book> pageContent;
		long nbRow;

		nbRow = this.findNbRow(Book.class); // nombre d'enregistrement de l'entité
		pageContent = this.findByPage(Book.class, pageSize, pageNumber); // les enregistrements a l'emplacement spécifié

		page = new Page<Book>(pageSize, pageNumber, nbRow, pageContent);

		return page;
	}
	
	
	/**
	 * 
	 * @param bookId
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<Note> findBookNotesByPage(long bookId, int pageSize, int pageNumber) throws Exception {

		// recherche des notes d'un book
		Query query = em.createQuery("SELECT n FROM Note n WHERE n.book.id = :p_book").setParameter("p_book", bookId)
				.setMaxResults(pageSize).setFirstResult(pageNumber*pageSize);
		List<Note> pageContent = query.getResultList();

		query = em.createQuery("SELECT count(*) FROM Note n WHERE n.book.id = :p_book").setParameter("p_book", bookId);
		long nbRow = (long) query.getSingleResult();
				
		Page<Note> page = new Page<Note>(pageSize, pageNumber, nbRow, pageContent);
		return page;

	}
	
	public List<Note> findBookNotes(long bookId) throws Exception {

		// recherche des notes d'un book
		Query query = em.createQuery("SELECT n FROM Note n WHERE n.book.id = :p_book").setParameter("p_book", bookId);
		List<Note> pageContent = (List<Note>) query.getResultList();

		return pageContent;
	}
	
	/**
	 * 
	 * @param bookId
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<Viewing> findBookViewingsByPage(long bookId, int pageSize, int pageNumber) throws Exception {

		// recherche des viewing d'un user
		Query query = em.createQuery("SELECT v FROM Viewing v WHERE v.book.id = :p_book").setParameter("p_book", bookId)
				.setMaxResults(pageSize).setFirstResult(pageNumber*pageSize);
		List<Viewing> pageContent = query.getResultList();

		query = em.createQuery("SELECT count(*) FROM Viewing v WHERE v.book.id = :p_book").setParameter("p_book", bookId);
		long nbRow = (long) query.getSingleResult();
				
		Page<Viewing> page = new Page<Viewing>(pageSize, pageNumber, nbRow, pageContent);
		return page;

	}

	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Book findBookById(long id) throws Exception {

		Book book = null;

		book = this.find(Book.class, id);

		return book;

	}

	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Book> findAllBooks() throws Exception {

		List<Book> books;
		books = this.findAll(Book.class);
		for (Book book : books) {		
			book.setAverageNote(this.getAverageNoteBook(book.getId()));
		}		
		
		return books;
	}

	
	/**
	 * retour en liste (plus utilise normalement)
	 * 
	 * @param bookName
	 * @return
	 * @throws Exception
	 */
	public List<Book> findBooksByTitleOrIsbn(String bookNameOrIsbn) throws Exception {
		List<Book> theBooks;
		bookNameOrIsbn = "%" + bookNameOrIsbn + "%";

		String requete = "SELECT b FROM Book b WHERE b.title like :p_book or b.isbn like :p_book";
		theBooks = (List<Book>) em.createQuery(requete).setParameter("p_book", bookNameOrIsbn).getResultList();

		return theBooks;
	}

	/**
	 * 
	 * @param bookId
	 * @return
	 * @throws Exception
	 */
	public long getAverageNoteBook (long bookId) throws Exception {
		long averageNote = 0;

		Query query = em.createQuery("SELECT sum(n.noteValue)/ count(*) FROM Note n WHERE n.book.id = :p_book ").setParameter("p_book", bookId);
		if (!(query.getSingleResult()== null)) 		
			averageNote = (long) query.getSingleResult();		

		return averageNote;
	}
	
	
	

	
}
