package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.exceptions.LibraryException;

@Stateless
@LocalBean
public class OrderLineDAO extends GenericDAO <OrderLine>{
	
	public OrderLineDAO()  {super();}

	/**
	 * 
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	public List<OrderLine> findOrderLinesOrder(Order order) {
		
		String requete = "SELECT o FROM OrderLine o WHERE o.order = :p_order"; 
		return  em.createQuery(requete).setParameter("p_order", order).getResultList(); 
	}
	public List<OrderLine> findOrderLinesOrder(long id) {
		
		String requete = "SELECT o FROM OrderLine o WHERE o.order.id = :p_orderId"; 
		return  em.createQuery(requete).setParameter("p_orderId", id).getResultList(); 
	}

}
