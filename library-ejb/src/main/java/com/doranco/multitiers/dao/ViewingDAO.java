package com.doranco.multitiers.dao;


import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;

@Stateless
@LocalBean
public class ViewingDAO  extends GenericDAO <Viewing> {
		
	
	public ViewingDAO()  {super();}
	
	/**
	 * 
	 * @param userId
	 * @param bookId
	 * @return
	 * @throws Exception
	 */
	public Viewing findViewingsByIds (long userId, long bookId )throws LibraryException {

		Viewing viewing; 
		
		String query = "Select v FROM Viewing v WHERE v.user.id =:p_user AND v.book.id = :p_book"; 
		viewing = (Viewing) em.createQuery(query).setParameter("p_user", userId).setParameter("p_book", bookId).getSingleResult();
		
		
		return viewing; 
		
	}	
	
	

	/**
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<Viewing> findViewingsByPage(int pageSize, int pageNumber) throws Exception {

		Page<Viewing> page;
		List<Viewing> pageContent;
		long nbRow;

		nbRow = this.findNbRow(Viewing.class); // nombre d'enregistrement de l'entité
		pageContent = this.findByPage(Viewing.class, pageSize, pageNumber); // les enregistrements a l'emplacement spécifié

	
		page = new Page<Viewing>(pageSize, pageNumber, nbRow, pageContent);

		return page;
	}	
	
	
	
	
}
