package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.entity.Viewing;
import com.doranco.multitiers.utils.Page;

@Stateless
@LocalBean
public class UserDAO extends GenericDAO<User> {

	private static Logger logger = Logger.getLogger(UserDAO.class);

	/**
	 * 
	 */
	public UserDAO() {
		super();
	}

	/**
	 * findUserByConnection
	 * 
	 * public User findUserByConnection paramètres : String userName, String
	 * password connection d'un utilisateur
	 */
	public User findUserByConnection(String userName, String password) throws NoResultException {

		User theUser;
		String requete = "SELECT u FROM User u WHERE u.userName=:p_userName AND u.password=:p_pass";
		theUser = (User) em.createQuery(requete).setParameter("p_userName", userName).setParameter("p_pass", password)
				.getSingleResult();

		return theUser;
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public User findUserById(long id) throws Exception {

		User user = null;

		user = this.find(User.class, id);

		return user;
	}

	/**
	 * 
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	public User findUserByUserName(String userName) throws Exception {

		User theUser;
		String requete = "SELECT u FROM User u WHERE u.userName=:p_userName";
		theUser = (User) em.createQuery(requete).setParameter("p_userName", userName).getSingleResult();

		return theUser;
	}

	/**
	 * findAllUser
	 * 
	 * Affiche la liste des Utilisateurs
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<User> findAllUser() throws Exception {

		List<User> users;

		// String requete = "SELECT u FROM User u ORDER BY id ASC ";
		// users = em.createQuery(requete).getResultList();

		users = this.findAll(User.class);

		return users;
	}

	/**
	 * findByPage
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	public Page<User> findUsersByPage(int pageSize, int pageNumber) throws Exception {

		Page<User> page;

		List<User> pageContent;
		long nbRow;

		nbRow = this.findNbRow(User.class); // nombre d'enregistrement de l'entité
		pageContent = this.findByPage(User.class, pageSize, pageNumber); // les enregistrements a l'emplacement spécifié

		page = new Page<User>(pageSize, pageNumber, nbRow, pageContent);

		return page;
	}

	/**
	 * findUserNotesByPage
	 * 
	 * @param userId
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<Note> findUserNotesByPage(long userId, int pageSize, int pageNumber) throws Exception {

		Query query ; 
		List<Note> pageContent = null;
		long nbRow = 0;
		// recherche des notes d'un user
		query = em.createQuery("SELECT n FROM Note n WHERE n.user.id=:p_user").setParameter("p_user", userId)
				.setMaxResults(pageSize).setFirstResult(pageNumber * pageSize);

		pageContent = query.getResultList();

		query = em.createQuery("SELECT count(*) FROM Note n WHERE n.user.id=:p_user").setParameter("p_user",userId);
		nbRow = (long) query.getSingleResult();

		Page<Note> page = new Page<Note>(pageSize, pageNumber, nbRow, pageContent);
		return page;

	}

	/**
	 * findUserViewingsByPage
	 * 
	 * @param userId
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<Viewing> findUserViewingsByPage(long userId, int pageSize, int pageNumber) throws Exception {

		// recherche des viewing d'un user
		Query query = em.createQuery("SELECT v FROM Viewing v WHERE v.user.id = :p_user").setParameter("p_user", userId)
				.setMaxResults(pageSize).setFirstResult(pageNumber * pageSize);
		List<Viewing> pageContent = query.getResultList();

		query = em.createQuery("SELECT count(*) FROM Viewing v WHERE v.user.id = :p_user").setParameter("p_user",
				userId);
		long nbRow = (long) query.getSingleResult();

		Page<Viewing> page = new Page<Viewing>(pageSize, pageNumber, nbRow, pageContent);
		return page;

	}

	/**
	 * findUserOrdersByPage
	 * 
	 * @param userId
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<Order> findUserOrdersByPage(long userId, int pageSize, int pageNumber) throws Exception {

		// recherche des orders d'un user
		Query query = em.createQuery("SELECT o FROM Order v WHERE o.user.id = :p_user").setParameter("p_user", userId)
				.setMaxResults(pageSize).setFirstResult(pageNumber * pageSize);
		List<Order> pageContent = query.getResultList();

		query = em.createQuery("SELECT count(o.id) FROM Order o WHERE o.user.id = :p_user").setParameter("p_user",
				userId);
		long nbRow = (long) query.getSingleResult();

		Page<Order> page = new Page<Order>(pageSize, pageNumber, nbRow, pageContent);
		return page;

	}

}
