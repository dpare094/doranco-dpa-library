package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.entity.OrderLine;
import com.doranco.multitiers.utils.Page;


@Stateless
@LocalBean
public class OrderDAO extends GenericDAO <Order>{

	/**
	 * 
	 */
	public OrderDAO()  {super();}
		
	
	
	/**
	 * 
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<Order> findOrdersByPage(int pageSize, int pageNumber) throws Exception {

		long nbRow = this.findNbRow(Order.class); 
		List<Order> pageContent = this.findByPage(Order.class, pageSize, pageNumber); 
		
		Page<Order> page = new Page<Order>(pageSize, pageNumber, nbRow, pageContent);

		return page;
	}

	/**
	 * 
	 * @param orderId
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	public Page<OrderLine> findOrderLinesOrderByPage(long orderId, int pageSize, int pageNumber)  throws Exception {

		// recherche des Orderlines d'un order
		Query query = em.createQuery("SELECT o FROM OrderLine o WHERE o.order.id = :p_line").setParameter("p_line", orderId)
				.setMaxResults(pageSize).setFirstResult(pageNumber*pageSize);
		List<OrderLine> pageContent = query.getResultList();

		query = em.createQuery("SELECT count(*) FROM OrderLine o WHERE o.order.id = :p_line").setParameter("p_line", orderId);
		long nbRow = (long) query.getSingleResult();
				
		Page<OrderLine> page = new Page<OrderLine> (pageSize, pageNumber, nbRow, pageContent);
		
		return page;

	}

	
}
